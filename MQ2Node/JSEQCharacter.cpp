#include "JSEQCharacter.h"
#include "Global.h"
#include "JSXTarget.h"

// hash fn
//  r.split(',').map(s =>s.trim()).map(s => `if (inString ==
//  "${s.slice(1).charAt(0).toLowerCase() + s.slice(2)}") return
//  ${s};`).join('\r\n')

// Case fn
// r.split(',').map(s =>s.trim()).map(s => `case ${s}:
// val = cast_int(char_info->);
// break;`).join('\r\n')

// For array init
// r.split('\n').map(a =>
// a.trim().replace(',','')).map(s=>`${s.slice(1).charAt(0).toLowerCase() +
// s.slice(2)}`).map(s =>`"${s}"`).join(',')

namespace jschar {
	enum string_code {

		eName,
		eLastName,
		eSTR,
		eSTA,
		eCHA,
		eDEX,
		eINT,
		eAGI,
		eWIS,
		eGuildId,
		eAAExp,
		eTributeTimer,
		eBenefitTimer,
		eCareerFavor,
		eCurrFavor,
		eGroupLeadershipExp,
		eRaidLeadershipExp,
		eGroupLeadershipPoints,
		eRaidLeadershipPoints,
		eRadiantCrystals,
		eEbonCrystals,
		eExp,
		eCurrWeight,
		eHpBonus,
		eManaBonus,
		eEnduranceBonus,
		eCombatEffectsCap,
		eShieldingCap,
		eSpellShieldCap,
		eAvoidanceCap,
		eAccuracyCap,
		eStunResistCap,
		eStrikeThroughCap,
		eSkillMinDamageModBonus,
		eDotShieldCap,
		eDamageShieldMitigationCap,
		eCombatEffectsBonus,
		eSpellShieldBonus,
		eShieldingBonus,
		eDamageShieldBonus,
		eDotShieldBonus,
		eDamageShieldMitigationBonus,
		eAvoidanceBonus,
		eAccuracyBonus,
		eStunResistBonus,
		eStrikeThroughBonus,
		eHeroicStrBonus,
		eHeroicIntBonus,
		eHeroicWisBonus,
		eHeroicAgiBonus,
		eHeroicDexBonus,
		eHeroicStaBonus,
		eHeroicChaBonus,
		eHeroicSvMagic,
		eHeroicSvFire,
		eHeroicSvCold,
		eHeroicSvDisease,
		eHeroicSvPoison,
		eHeroicSvCorruption,
		eHealAmountBonus,
		eSpellDamageBonus,
		eClairvoyanceBonus,
		eAttackBonus,
		eHpRegenBonus,
		eManaRegenBonus,
		eEnduranceRegenBonus,
		eAttackSpeed,
		eInCombat,
		eDownTime,
		eDownTimeStamp,
		eStunned,
		eZoneId,
		eInstance,
		eStandState,
		eBankSharedPlat,
		eBankSharedGold,
		eBankSharedSilver,
		eBankSharedCopper,
		eBankPlat,
		eBankGold,
		eBankSilver,
		eBankCopper,
		eSavePoison,
		eSaveMagic,
		eSaveDisease,
		eSaveCorruption,
		eSaveFire,
		eSaveCold,

		// Structs
		eXTarget,
		eSpawn,
		eTarget,
		eGroup,

		// CharInfo2 - let's just concat everything into one object
		eBuffs
		///*0x1da0*/ struct _LEADERABILITIES
		//    MyAbilities;  // points spent in each ability (size 0x3c)
		// /*0x1ea8*/ struct _BANKARRAY* pBankArray;
		///*0x1ee0*/ struct _SHAREDBANKARRAY* pSharedBankArray;
		///*0x2dcc*/ struct _EQC_INFO* eqc_info;

	};
}


jschar::string_code hashit(std::string const& inString) {
	if (inString == "name") return jschar::eName;
	if (inString == "str") return jschar::eSTR;
	if (inString == "sta") return jschar::eSTA;
	if (inString == "cha") return jschar::eCHA;
	if (inString == "dex") return jschar::eDEX;
	if (inString == "int") return jschar::eINT;
	if (inString == "agi") return jschar::eAGI;
	if (inString == "wis") return jschar::eWIS;
	if (inString == "guildId") return jschar::eGuildId;
	if (inString == "aaExp") return jschar::eAAExp;
	if (inString == "tributeTimer") return jschar::eTributeTimer;
	if (inString == "benefitTimer") return jschar::eBenefitTimer;
	if (inString == "careerFavor") return jschar::eCareerFavor;
	if (inString == "currFavor") return jschar::eCurrFavor;
	if (inString == "groupLeadershipExp") return jschar::eGroupLeadershipExp;
	if (inString == "raidLeadershipExp") return jschar::eRaidLeadershipExp;
	if (inString == "groupLeadershipPoints") return jschar::eGroupLeadershipPoints;
	if (inString == "raidLeadershipPoints") return jschar::eRaidLeadershipPoints;
	if (inString == "radiantCrystals") return jschar::eRadiantCrystals;
	if (inString == "ebonCrystals") return jschar::eEbonCrystals;
	if (inString == "exp") return jschar::eExp;
	if (inString == "currWeight") return jschar::eCurrWeight;
	if (inString == "hpBonus") return jschar::eHpBonus;
	if (inString == "manaBonus") return jschar::eManaBonus;
	if (inString == "enduranceBonus") return jschar::eEnduranceBonus;
	if (inString == "combatEffectsCap") return jschar::eCombatEffectsCap;
	if (inString == "shieldingCap") return jschar::eShieldingCap;
	if (inString == "spellShieldCap") return jschar::eSpellShieldCap;
	if (inString == "avoidanceCap") return jschar::eAvoidanceCap;
	if (inString == "accuracyCap") return jschar::eAccuracyCap;
	if (inString == "stunResistCap") return jschar::eStunResistCap;
	if (inString == "strikeThroughCap") return jschar::eStrikeThroughCap;
	if (inString == "skillMinDamageModBonus") return jschar::eSkillMinDamageModBonus;
	if (inString == "dotShieldCap") return jschar::eDotShieldCap;
	if (inString == "damageShieldMitigationCap")
		return jschar::eDamageShieldMitigationCap;
	if (inString == "combatEffectsBonus") return jschar::eCombatEffectsBonus;
	if (inString == "spellShieldBonus") return jschar::eSpellShieldBonus;
	if (inString == "shieldingBonus") return jschar::eShieldingBonus;
	if (inString == "damageShieldBonus") return jschar::eDamageShieldBonus;
	if (inString == "dotShieldBonus") return jschar::eDotShieldBonus;
	if (inString == "damageShieldMitigationBonus")
		return jschar::eDamageShieldMitigationBonus;
	if (inString == "avoidanceBonus") return jschar::eAvoidanceBonus;
	if (inString == "accuracyBonus") return jschar::eAccuracyBonus;
	if (inString == "stunResistBonus") return jschar::eStunResistBonus;
	if (inString == "strikeThroughBonus") return jschar::eStrikeThroughBonus;
	if (inString == "heroicStrBonus") return jschar::eHeroicStrBonus;
	if (inString == "heroicIntBonus") return jschar::eHeroicIntBonus;
	if (inString == "heroicWisBonus") return jschar::eHeroicWisBonus;
	if (inString == "heroicAgiBonus") return jschar::eHeroicAgiBonus;
	if (inString == "heroicDexBonus") return jschar::eHeroicDexBonus;
	if (inString == "heroicStaBonus") return jschar::eHeroicStaBonus;
	if (inString == "heroicChaBonus") return jschar::eHeroicChaBonus;
	if (inString == "heroicSvMagic") return jschar::eHeroicSvMagic;
	if (inString == "heroicSvFire") return jschar::eHeroicSvFire;
	if (inString == "heroicSvCold") return jschar::eHeroicSvCold;
	if (inString == "heroicSvDisease") return jschar::eHeroicSvDisease;
	if (inString == "heroicSvPoison") return jschar::eHeroicSvPoison;
	if (inString == "heroicSvCorruption") return jschar::eHeroicSvCorruption;
	if (inString == "healAmountBonus") return jschar::eHealAmountBonus;
	if (inString == "spellDamageBonud") return jschar::eSpellDamageBonus;
	if (inString == "clairvoyanceBonus") return jschar::eClairvoyanceBonus;
	if (inString == "attackBonus") return jschar::eAttackBonus;
	if (inString == "hpRegenBonus") return jschar::eHpRegenBonus;
	if (inString == "manaRegenBonus") return jschar::eManaRegenBonus;
	if (inString == "enduranceRegenBonus") return jschar::eEnduranceRegenBonus;
	if (inString == "attackSpeed") return jschar::eAttackSpeed;
	if (inString == "inCombat") return jschar::eInCombat;
	if (inString == "downTime") return jschar::eDownTime;
	if (inString == "downTimeStamp") return jschar::eDownTimeStamp;
	if (inString == "stunned") return jschar::eStunned;
	if (inString == "zoneId") return jschar::eZoneId;
	if (inString == "instance") return jschar::eInstance;
	if (inString == "standState") return jschar::eStandState;
	if (inString == "bankSharedPlat") return jschar::eBankSharedPlat;
	if (inString == "bankSharedGold") return jschar::eBankSharedGold;
	if (inString == "bankSharedSilver") return jschar::eBankSharedSilver;
	if (inString == "bankSharedCopper") return jschar::eBankSharedCopper;
	if (inString == "bankPlat") return jschar::eBankPlat;
	if (inString == "bankGold") return jschar::eBankGold;
	if (inString == "bankSilver") return jschar::eBankSilver;
	if (inString == "bankCopper") return jschar::eBankCopper;
	if (inString == "savePoison") return jschar::eSavePoison;
	if (inString == "saveMagic") return jschar::eSaveMagic;
	if (inString == "saveDisease") return jschar::eSaveDisease;
	if (inString == "saveCorruption") return jschar::eSaveCorruption;
	if (inString == "saveFire") return jschar::eSaveFire;
	if (inString == "saveCold") return jschar::eSaveCold;

	// Structs
	if (inString == "xTarget") return jschar::eXTarget;
	if (inString == "target") return jschar::eTarget;
	if (inString == "spawn") return jschar::eSpawn;
	if (inString == "group") return jschar::eGroup;

	// Arrays
	if (inString == "buffs") return jschar::eBuffs;
}

v8::AccessorNameGetterCallback GetCharAttribute() {
	struct _GROUPINFO* pGroupInfo;
	return [](Local<v8::Name> name, const v8::PropertyCallbackInfo<Value>& info) {
		auto char_info = GetCharInfo();
		auto isolate = info.GetIsolate();
		v8::Local<v8::Value> val = cast_to_js(info, "Undefined");

		v8::String::Utf8Value accessor(info.GetIsolate(), name);
		switch (hashit(std::string(*accessor))) {
		case jschar::eName:
			val = cast_to_js(info, char_info->Name);
			break;
		case jschar::eSTR:
			val = cast_to_js(info, char_info->STR);
			break;
		case jschar::eSTA:
			val = cast_to_js(info, char_info->STA);
			break;
		case jschar::eCHA:
			val = cast_to_js(info, char_info->CHA);
			break;
		case jschar::eDEX:
			val = cast_to_js(info, char_info->DEX);
			break;
		case jschar::eINT:
			val = cast_to_js(info, char_info->INT);
			break;
		case jschar::eAGI:
			val = cast_to_js(info, char_info->AGI);
			break;
		case jschar::eWIS:
			val = cast_to_js(info, char_info->WIS);
			break;
		case jschar::eAAExp:
			val = cast_to_js(info, char_info->AAExp);
			break;
		case jschar::eTributeTimer:
			val = cast_to_js(info, char_info->TributeTimer);
			break;
		case jschar::eBenefitTimer:
			val = cast_to_js(info, char_info->BenefitTimer);
			break;
		case jschar::eCareerFavor:
			val = cast_to_js(info, char_info->CareerFavor);
			break;
		case jschar::eCurrFavor:
			val = cast_to_js(info, char_info->CurrFavor);
			break;
		case jschar::eRadiantCrystals:
			val = cast_to_js(info, char_info->RadiantCrystals);
			break;
		case jschar::eEbonCrystals:
			val = cast_to_js(info, char_info->EbonCrystals);
			break;
		case jschar::eExp:
			val = cast_to_js(info, char_info->Exp);
			break;
		case jschar::eCurrWeight:
			val = cast_to_js(info, char_info->CurrWeight);
			break;
		case jschar::eHpBonus:
			val = cast_to_js(info, char_info->HPBonus);
			break;
		case jschar::eManaBonus:
			val = cast_to_js(info, char_info->ManaBonus);
			break;
		case jschar::eEnduranceBonus:
			val = cast_to_js(info, char_info->EnduranceBonus);
			break;
		case jschar::eCombatEffectsBonus:
			val = cast_to_js(info, char_info->CombatEffectsBonus);
			break;
		case jschar::eSpellShieldBonus:
			val = cast_to_js(info, char_info->SpellShieldBonus);
			break;
		case jschar::eShieldingBonus:
			val = cast_to_js(info, char_info->ShieldingBonus);
			break;
		case jschar::eDamageShieldBonus:
			val = cast_to_js(info, char_info->DamageShieldBonus);
			break;
		case jschar::eDotShieldBonus:
			val = cast_to_js(info, char_info->DoTShieldBonus);
			break;
		case jschar::eDamageShieldMitigationBonus:
			val = cast_to_js(info, char_info->DamageShieldMitigationBonus);
			break;
		case jschar::eAvoidanceBonus:
			val = cast_to_js(info, char_info->AvoidanceBonus);
			break;
		case jschar::eAccuracyBonus:
			val = cast_to_js(info, char_info->AccuracyBonus);
			break;
		case jschar::eStunResistBonus:
			val = cast_to_js(info, char_info->StunResistBonus);
			break;
		case jschar::eStrikeThroughBonus:
			val = cast_to_js(info, char_info->StrikeThroughBonus);
			break;
		case jschar::eHeroicStrBonus:
			val = cast_to_js(info, char_info->HeroicSTRBonus);
			break;
		case jschar::eHeroicIntBonus:
			val = cast_to_js(info, char_info->HeroicSTABonus);
			break;
		case jschar::eHeroicWisBonus:
			val = cast_to_js(info, char_info->HeroicWISBonus);
			break;
		case jschar::eHeroicAgiBonus:
			val = cast_to_js(info, char_info->HeroicAGIBonus);
			break;
		case jschar::eHeroicDexBonus:
			val = cast_to_js(info, char_info->HeroicDEXBonus);
			break;
		case jschar::eHeroicStaBonus:
			val = cast_to_js(info, char_info->HeroicSTABonus);
			break;
		case jschar::eHeroicChaBonus:
			val = cast_to_js(info, char_info->HeroicCHABonus);
			break;
		case jschar::eHealAmountBonus:
			val = cast_to_js(info, char_info->HealAmountBonus);
			break;
		case jschar::eSpellDamageBonus:
			val = cast_to_js(info, char_info->SpellDamageBonus);
			break;
		case jschar::eClairvoyanceBonus:
			val = cast_to_js(info, char_info->ClairvoyanceBonus);
			break;
		case jschar::eAttackBonus:
			val = cast_to_js(info, char_info->AttackBonus);
			break;
		case jschar::eHpRegenBonus:
			val = cast_to_js(info, char_info->HPRegenBonus);
			break;
		case jschar::eManaRegenBonus:
			val = cast_to_js(info, char_info->ManaRegenBonus);
			break;
		case jschar::eEnduranceRegenBonus:
			val = cast_to_js(info, char_info->EnduranceRegenBonus);
			break;
		case jschar::eAttackSpeed:
			val = cast_to_js(info, char_info->AttackSpeed);
			break;
		case jschar::eInCombat:
			val = cast_to_js(info, char_info->InCombat);
			break;
		case jschar::eDownTime:
			val = cast_to_js(info, char_info->Downtime);
			break;
		case jschar::eDownTimeStamp:
			val = cast_to_js(info, char_info->DowntimeStamp);
			break;
		case jschar::eStunned:
			val = cast_to_js(info, char_info->Stunned);
			break;
		case jschar::eZoneId:
			val = cast_to_js(info, char_info->zoneId);
			break;
		case jschar::eInstance:
			val = cast_to_js(info, char_info->instance);
			break;
		case jschar::eStandState:
			val = cast_to_js(info, char_info->standstate);
			break;
		case jschar::eBankSharedPlat:
			val = cast_to_js(info, char_info->BankSharedPlat);
			break;
		case jschar::eBankPlat:
			val = cast_to_js(info, char_info->BankPlat);
			break;
		case jschar::eBankGold:
			val = cast_to_js(info, char_info->BankGold);
			break;
		case jschar::eBankSilver:
			val = cast_to_js(info, char_info->BankSilver);
			break;
		case jschar::eBankCopper:
			val = cast_to_js(info, char_info->BankCopper);
			break;
		case jschar::eSavePoison:
			val = cast_to_js(info, char_info->SavePoison);
			break;
		case jschar::eSaveMagic:
			val = cast_to_js(info, char_info->SaveMagic);
			break;
		case jschar::eSaveDisease:
			val = cast_to_js(info, char_info->SaveDisease);
			break;
		case jschar::eSaveCorruption:
			val = cast_to_js(info, char_info->SaveCorruption);
			break;
		case jschar::eSaveFire:
			val = cast_to_js(info, char_info->SaveFire);
			break;
		case jschar::eSaveCold:
			val = cast_to_js(info, char_info->SaveCold);
			break;

			// Structs
		case jschar::eXTarget:
			val = GetXTarget(char_info->pXTargetMgr, info);
			break;
		case jschar::eTarget:
			val = GetSpawn((PSPAWNINFO)*ppTarget, isolate);
			break;
		case jschar::eSpawn:
			val = GetSpawn(char_info->pSpawn, isolate);
			break;
		case jschar::eGroup:
			val = GetGroupInfo(char_info->pGroupInfo, info);
			break;

			// Arrays
		case jschar::eBuffs:
		{
			v8::Local<v8::Array> buff_array = v8::Array::New(isolate);
			for (int i = 0; i < NUM_LONG_BUFFS; i++) {
				auto buff = GetCharInfo2()->Buff[i];
				if (buff.SpellID > 0) {
					buff_array->Set(isolate->GetCurrentContext(), buff_array->Length(), GetBuff((PSPELLBUFF)&GetCharInfo2()->Buff[i], info));
				}
			}
			for (int i = 0; i < NUM_SHORT_BUFFS; i++) {
				auto buff = GetCharInfo2()->Buff[i];
				if (buff.SpellID > 0) {
					buff_array->Set(isolate->GetCurrentContext(), buff_array->Length(), GetBuff((PSPELLBUFF)&GetCharInfo2()->Buff[i], info));
				}
			}
			val = buff_array;
		}

		break;

		// ROF2
#ifdef ROF2
		case jschar::eGroupLeadershipExp:
			val = cast_to_js(info, char_info->GroupLeadershipExp);
			break;
		case jschar::eRaidLeadershipExp:
			val = cast_to_js(info, char_info->#RaidLeadershipExp);
			break;
		case jschar::eGroupLeadershipPoints:
			val = cast_to_js(info, char_info->GroupLeadershipPoints);
			break;
		case jschar::eRaidLeadershipPoints:
			val = cast_to_js(info, char_info->RaidLeadershipPoints);
			break;
		case jschar::eCombatEffectsCap:
			val = cast_to_js(info, char_info->CombatEffectsCap);
			break;
		case jschar::eShieldingCap:
			val = cast_to_js(info, char_info->ShieldingCap);
			break;
		case jschar::eSpellShieldCap:
			val = cast_to_js(info, char_info->SpellShieldCap);
			break;
		case jschar::eAvoidanceCap:
			val = cast_to_js(info, char_info->AvoidanceCap);
			break;
		case jschar::eAccuracyCap:
			val = cast_to_js(info, char_info->AccuracyCap);
			break;
		case jschar::eStunResistCap:
			val = cast_to_js(info, char_info->StunResistCap);
			break;
		case jschar::eStrikeThroughCap:
			val = cast_to_js(info, char_info->StrikeThroughCap);
			break;
		case jschar::eSkillMinDamageModBonus:
			val = cast_to_js(info, "Undefined");
			break;
		case jschar::eDotShieldCap:
			val = cast_to_js(info, char_info->DoTShieldCap);
			break;
		case jschar::eDamageShieldMitigationCap:
			val = cast_to_js(info, char_info->DamageShieldMitigationCap);
			break;
		case jschar::eHeroicSvMagic:
			val = cast_to_js(info, char_info->HeroicSvMagicBonus);
			break;
		case jschar::eHeroicSvFire:
			val = cast_to_js(info, char_info->HeroicSvFireBonus);
			break;
		case jschar::eHeroicSvCold:
			val = cast_to_js(info, char_info->HeroicSvColdBonus);
			break;
		case jschar::eHeroicSvDisease:
			val = cast_to_js(info, char_info->HeroicSvDiseaseBonus);
			break;
		case jschar::eHeroicSvPoison:
			val = cast_to_js(info, char_info->HeroicSvPoisonBonus);
			break;
		case jschar::eHeroicSvCorruption:
			val = cast_to_js(info, char_info->HeroicSvCorruptionBonus);
			break;
		case jschar::eBankSharedGold:
			val = cast_to_js(info, char_info->BankSharedGold);
			break;
		case jschar::eBankSharedSilver:
			val = cast_to_js(info, char_info->BankSharedSilver);
			break;
		case jschar::eBankSharedCopper:
			val = cast_to_js(info, char_info->BankSharedCopper);
			break;
#else
#endif

		default:
			val = cast_to_js(info, "Undefined");
			break;
		}
		info.GetReturnValue().Set(val);
	};
};

v8::AccessorNameGetterCallback GetPlayerObject() {
	return [](Local<v8::Name> name, const v8::PropertyCallbackInfo<Value>& info) {
		auto isolate = info.GetIsolate();
		auto context = isolate->GetCurrentContext();
		auto obj = v8::Object::New(isolate);
		auto pChar = GetCharInfo();
		std::function<void(const char*)> set_accessor =
			[context, obj, isolate](const char* accessor) -> void {
			obj->SetAccessor(
				context, GetLocalName(isolate, accessor), GetCharAttribute());
		};
		if (pChar != nullptr) {
			std::vector<std::string> attribute_list{ "name",
													"str",
													"sta",
													"cha",
													"dex",
													"int",
													"agi",
													"wis",
													"guildId",
													"aaExp",
													"tributeTimer",
													"benefitTimer",
													"careerFavor",
													"currFavor",
													"groupLeadershipExp",
													"raidLeadershipExp",
													"groupLeadershipPoints",
													"raidLeadershipPoints",
													"radiantCrystals",
													"ebonCrystals",
													"exp",
													"currWeight",
													"hpBonus",
													"manaBonus",
													"enduranceBonus",
													"combatEffectsCap",
													"shieldingCap",
													"spellShieldCap",
													"avoidanceCap",
													"accuracyCap",
													"stunResistCap",
													"strikeThroughCap",
													"skillMinDamageModBonus",
													"dotShieldCap",
													"damageShieldMitigationCap",
													"combatEffectsBonus",
													"spellShieldBonus",
													"shieldingBonus",
													"damageShieldBonus",
													"dotShieldBonus",
													"damageShieldMitigationBonus",
													"avoidanceBonus",
													"accuracyBonus",
													"stunResistBonus",
													"strikeThroughBonus",
													"heroicStrBonus",
													"heroicIntBonus",
													"heroicWisBonus",
													"heroicAgiBonus",
													"heroicDexBonus",
													"heroicStaBonus",
													"heroicChaBonus",
													"heroicSvMagic",
													"heroicSvFire",
													"heroicSvCold",
													"heroicSvDisease",
													"heroicSvPoison",
													"heroicSvCorruption",
													"healAmountBonus",
													"spellDamageBonud",
													"clairvoyanceBonus",
													"attackBonus",
													"hpRegenBonus",
													"manaRegenBonus",
													"enduranceRegenBonus",
													"attackSpeed",
													"inCombat",
													"downTime",
													"downTimeStamp",
													"stunned",
													"zoneId",
													"instance",
													"standState",
													"bankSharedPlat",
													"bankSharedGold",
													"bankSharedSilver",
													"bankSharedCopper",
													"bankPlat",
													"bankGold",
													"bankSilver",
													"bankCopper",
													"savePoison",
													"saveMagic",
													"saveDisease",
													"saveCorruption",
													"saveFire",
													"saveCold",

				// structs
				"xTarget",
				"spawn",
				"target",
				"group",

				// arrays
				"buffs"

			};
			for (std::string attribute : attribute_list) {
				set_accessor(attribute.c_str());
			}
		}

		info.GetReturnValue().Set(obj);
	};
}
