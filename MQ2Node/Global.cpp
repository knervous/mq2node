#include "Global.h"
#include "JSEQCharacter.h"


v8::Local<v8::String> GetLocalName(v8::Isolate* isolate, const char* val) {
	return v8::String::NewFromUtf8(isolate, val, v8::NewStringType::kInternalized)
		.ToLocalChecked();
}

void GetZoneSpawns(const v8::FunctionCallbackInfo<Value>& info) {
	PSPAWNINFO pAllSpawns = (PSPAWNINFO)pSpawnList;
	auto isolate = info.GetIsolate();
	auto arr = v8::Array::New(isolate);
	int i = 0;
	while (pAllSpawns)
	{
		arr->Set(isolate->GetCurrentContext(), i++, GetSpawn(pAllSpawns, isolate));
		pAllSpawns = pAllSpawns->pNext;
	}
	info.GetReturnValue().Set(arr);
}

bool is_number(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}


void MqEval(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0) {
		auto eval = info[0]->ToString(context).ToLocalChecked();
		String::Utf8Value eval_utf8(info.GetIsolate(), eval);
		const char* str = *eval_utf8;
		size_t Size = strlen(str);

		auto eval_string = ParseMacroParameter(GetCharInfo()->pSpawn, (PCHAR)str, Size);

		// Try to coerce values through int, float, bool, then fallback to string
		if (is_number(std::string(eval_string))) {
			info.GetReturnValue().Set(v8::Integer::New(info.GetIsolate(), std::atoi(eval_string)));
			return;
		}
		else if (0 == strcmp(eval_string, "TRUE")) {
			info.GetReturnValue().Set(v8::Boolean::New(info.GetIsolate(), true));
			return;
		}
		else if (0 == strcmp(eval_string, "FALSE")) {
			info.GetReturnValue().Set(v8::Boolean::New(info.GetIsolate(), false));
			return;
		}

		info.GetReturnValue().Set(GetLocalName(info.GetIsolate(), eval_string));
	}
}

void ConsoleLog(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0) {
		auto log = info[0]->ToString(context).ToLocalChecked();
		String::Utf8Value log_utf8(info.GetIsolate(), log);
		WriteChatColor(*log_utf8);
		std::printf(*log_utf8);
	}
}

void ConsoleWarn(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0) {
		auto log = info[0]->ToString(context).ToLocalChecked();
		String::Utf8Value log_utf8(info.GetIsolate(), log);
		WriteChatColor(*log_utf8, CONCOLOR_YELLOW);
		std::printf(*log_utf8);
	}
}

void RunCommand(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0 && info[0]->IsString()) {
		auto command = info[0]->ToString(context).ToLocalChecked();
		String::Utf8Value command_utf8(info.GetIsolate(), command);
		std::string command_str("/");
		command_str += *command_utf8;
		try {
			DoCommand(GetCharInfo()->pSpawn, (PCHAR)command_str.c_str());
		}
		catch (...) {
			return;
		}
	}
}

void GetSpawnById(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0 && info[0]->IsInt32()) {
		auto id = info[0]->Int32Value(context).FromJust();
		auto spawn = GetSpawnByID(id);
		if (spawn) {
			info.GetReturnValue().Set(GetSpawn(&spawn->Data, info.GetIsolate()));
		}
	}
	else {
		WriteChatColor("Bad args for getSpawnById, arg 1 is int", CONCOLOR_YELLOW);
	}
}

void GetNthNearestSpawn(const v8::FunctionCallbackInfo<Value>& info) {
	auto context = info.GetIsolate()->GetCurrentContext();
	if (info.kArgsLength > 0) {
		if (!info[0]->IsString()) {
			WriteChatColor("Bad args for getNthNearestSpawn, arg 1 is string and opt arg 2 is int", CONCOLOR_YELLOW);
			return;
		}
		auto search = info[0]->ToString(context).ToLocalChecked();
		String::Utf8Value log_utf8(info.GetIsolate(), search);

		auto n = info.kArgsLength > 1 && info[1]->IsInt32() ? info[1]->Int32Value(context).FromJust() : 1;
		PSPAWNINFO spawn;
		SEARCHSPAWN ssSpawn;
		ClearSearchSpawn(&ssSpawn);
		ssSpawn.SpawnType = NPC;

		strcpy_s(ssSpawn.szName, *log_utf8);
		spawn = NthNearestSpawn(&ssSpawn, n, GetCharInfo()->pSpawn);
		info.GetReturnValue().Set(GetSpawn((_SPAWNINFO*)spawn, info.GetIsolate()));
	}
	else {
		WriteChatColor("Bad args for getNthNearestSpawn, arg 1 is string and opt arg 2 is int", CONCOLOR_YELLOW);
	}
}

void GetMqGlobal(Local<v8::String> name,
	const v8::PropertyCallbackInfo<Value>& info) {
	auto isolate = info.GetIsolate();
	auto context = isolate->GetCurrentContext();

	v8::Local<v8::External> ext = info.Data().As<v8::External>();

	auto obj = v8::Object::New(isolate);

	obj->SetAccessor(context, GetLocalName(isolate, "me"), GetPlayerObject());

	// Methods
	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "log"),
		v8::Function::New(context, ConsoleLog).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "warn"),
		v8::Function::New(context, ConsoleWarn).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "run"),
		v8::Function::New(context, RunCommand).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "eval"),
		v8::Function::New(context, MqEval).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "getSpawnById"),
		v8::Function::New(context, GetSpawnById).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "getZoneSpawns"),
		v8::Function::New(context, GetZoneSpawns).ToLocalChecked());

	obj->DefineOwnProperty(
		context,
		GetLocalName(isolate, "getNthNearestSpawn"),
		v8::Function::New(context, GetNthNearestSpawn).ToLocalChecked());


	info.GetReturnValue().Set(obj);
}

v8::Local<v8::ObjectTemplate> GetObjectTemplate(Isolate* isolate, NodeRuntime* node_runtime) {
	v8::Local<v8::ObjectTemplate> global_templ = v8::ObjectTemplate::New(isolate);

	global_templ->SetAccessor(GetLocalName(isolate, "mq"), GetMqGlobal, nullptr, v8::External::New(isolate, node_runtime));

	return global_templ;
}

