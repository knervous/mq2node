#pragma once
#include <type_traits>
#include "JSSpawn.h"
#include "NodeRuntime.h"
#include "JSSpell.h"
#include "JSBuff.h"

v8::Local<v8::String> GetLocalName(v8::Isolate* isolate, const char* key);

void RunCommand(const v8::FunctionCallbackInfo<Value>& info);

void GetCommands(Local<v8::Name> name,
	const v8::PropertyCallbackInfo<Value>& info);

void ConsoleLog(const v8::FunctionCallbackInfo<Value>& info);

void AddSpawnListener(Local<v8::Name> name,
	const v8::PropertyCallbackInfo<Value>& info);

void GetMqGlobal(Local<v8::String> name,
	const v8::PropertyCallbackInfo<Value>& info);

v8::Local<v8::ObjectTemplate> GetObjectTemplate(Isolate* isolate, NodeRuntime* node_runtime);

template <typename T>
v8::Local<v8::Value> cast_to_js(const v8::PropertyCallbackInfo<Value>& info,
	T val) {
	auto isolate = info.GetIsolate();
	try {
		if constexpr (std::is_same<T, DWORD>::value) {
			return v8::Integer::New(isolate, (int32_t)val);
		}
		else if constexpr (std::is_same<T, LONG>::value) {
			return v8::Integer::New(isolate, (long)val);
		}
		else if constexpr (std::is_same<T, int>::value) {
			return v8::Integer::New(isolate, (int32_t)val);
		}
		else if constexpr (std::is_same<T, long>::value) {
			return v8::Integer::New(isolate, (long)val);
		}
		else if constexpr (std::is_same<T, long long>::value) {
			return v8::Integer::New(isolate, (long)val);
		}
		else if constexpr (std::is_same<T, BYTE>::value) {
			return v8::Integer::New(isolate, (short)val);
		}
		else if constexpr (std::is_same<T, FLOAT>::value) {
			return v8::Number::New(isolate, (double)val);
		}
		else if constexpr (std::is_same<T, _SPAWNINFO*>::value) {
			return GetSpawn((_SPAWNINFO*)val, isolate);
		}
		else if constexpr (std::is_same<T, const char*>::value) {
			return GetLocalName(isolate, (const char*)val);
		}
		else if constexpr (std::is_same<T, CHAR>::value) {
			return GetLocalName(isolate, ((const char*)val));
		}
		else if constexpr (std::is_same<T, std::string>::value) {
			return GetLocalName(isolate, ((std::string)val).c_str());
		}
		else {
			return GetLocalName(isolate, ((const char*)val));
		}
	}
	catch (...) {
		return v8::Null(isolate);
	}
}
