#pragma once
#include "NodeRuntime.h"

#ifdef ROF2
v8::Local<v8::Object> GetXTarget(_XTARGETMGR* pXTargetMgr,
                                 const v8::PropertyCallbackInfo<Value>& info);

#else
v8::Local<v8::Array> GetXTarget(ExtendedTargetList* pXTargetMgr,
                                 const v8::PropertyCallbackInfo<Value>& info);
#endif
