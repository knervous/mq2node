#include "JSXTarget.h"
#include "Global.h"

namespace jsxtarget {
enum string_code { eName, eSpawnId, eTargetType, eSpawn };
}

jsxtarget::string_code hashit(std::string const& inString) {
  if (inString == "name") return jsxtarget::eName;
  if (inString == "targetType") return jsxtarget::eTargetType;
  if (inString == "spawnId") return jsxtarget::eSpawnId;
  if (inString == "spawn") return jsxtarget::eSpawn;
}

#ifdef ROF2
v8::AccessorNameGetterCallback GetTargetAttribute(_XTARGETMGR* pXTargetMgr) {
  static auto ret = [pXTargetMgr](Local<v8::Name> name,
                                  const v8::PropertyCallbackInfo<Value>& info) {
    auto isolate = info.GetIsolate();
    v8::Local<v8::Value> val;
    std::function<v8::Local<v8::Integer>(int32_t)> cast_int =
        [isolate](int32_t val) -> v8::Local<v8::Integer> {
      return v8::Integer::New(isolate, val);
    };
    std::function<v8::Local<v8::String>(CHAR*)> cast_string =
        [isolate](CHAR* val) -> v8::Local<v8::String> {
      return GetLocalName(isolate, val);
    };
    v8::String::Utf8Value accessor(info.GetIsolate(), name);
    if (pXTargetMgr->pXTargetArray == nullptr) {
      val = cast_string("No Target");
    } else {
      switch (hashit(std::string(*accessor))) {
        case jsxtarget::eName:
          val = cast_string(pXTargetMgr->pXTargetArray->pXTargetData->Name);
          break;
        case jsxtarget::eTargetType:
          val = cast_string(GetXtargetType(
              pXTargetMgr->pXTargetArray->pXTargetData->xTargetType));
          break;
        case jsxtarget::eSpawnId:
          val = cast_int(pXTargetMgr->pXTargetArray->pXTargetData->xTargetType);
          break;
        default:
          val = cast_string("Undefined");
          break;
      }
      info.GetReturnValue().Set(val);
    };
  };
  return [](Local<v8::Name> name, const v8::PropertyCallbackInfo<Value>& info) {
    return ret(name, info);
  };
}

v8::Local<v8::Object> GetXTarget(_XTARGETMGR* pXTargetMgr,
                                 const v8::PropertyCallbackInfo<Value>& info) {
  auto isolate = info.GetIsolate();
  auto context = isolate->GetCurrentContext();
  auto obj = v8::Object::New(isolate);
  std::function<void(const char*)> set_accessor =
      [context, obj, isolate, pXTargetMgr](const char* accessor) -> void {
    obj->SetAccessor(context,
                     GetLocalName(isolate, accessor),
                     GetTargetAttribute(pXTargetMgr));
  };
  if (pXTargetMgr != nullptr) {
    std::vector<std::string> attribute_list{
        "name", "targetType", "spawnId", "spawn"};
    for (std::string attribute : attribute_list) {
      set_accessor(attribute.c_str());
    }
  }
  return obj;
}
#else

v8::Local<v8::Array> GetXTarget(ExtendedTargetList* pXTargetMgr,
                                const v8::PropertyCallbackInfo<Value>& info) {
  auto isolate = info.GetIsolate();
  auto arr = v8::Array::New(isolate, pXTargetMgr->XTargetSlots.Count);

  for (int i = 0; i < pXTargetMgr->XTargetSlots.Count; i++) {
    // Todo use new EQPlayer class here maybe - do we want to expose those methods on it? Maybe not
    auto spawn_data = &GetSpawnByID(pXTargetMgr->XTargetSlots[i].SpawnID)->Data;
    arr->Set(isolate->GetCurrentContext(), i, GetSpawn(spawn_data, isolate));
  }

  return arr;
};
#endif
