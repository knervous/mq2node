#include "JSGroupInfo.h"
#include "Global.h"

// hash fn
//r.split(',')
//.map(s = > s.trim())
//.map(s = > `jsbuff::${ s }`)
//	.map(s = >
//{
//	const v = s.replace('jsbuff::', '');
//	return `if (inString == "${v.slice(1).charAt(0).toLowerCase() +
//		"
//		"v.slice(2)}") return ${ s };
//	`;
//})
//.join('\r\n');
//
//	 Case fn
//	r.split(',').map(s = > s.trim()).map(s = > `case jsbuff::${ s }:
//	val = cast_to_js(info, spawn_info->);
//	break; `).join('\r\n')
		//
		//// For array init
		//r.split('\n').map(a = > a.trim().replace(',', '')).map(s = > `${s.slice(1).charAt(0).toLowerCase() + s.slice(2)}`).map(s = > `"${s}"`).join(',')


namespace jsbuff {
	enum string_code {
		eSpellId,
		eSpell,
		eDuration,
		eInitialDuration,
		eHitCount,
		eModifier,
		eX,
		eY,
		eZ,
		eType,
		eCasterLevel
	};
}


jsbuff::string_code hashit(std::string const& inString) {
	if (inString == "spellId") return jsbuff::eSpellId;
	if (inString == "spell") return jsbuff::eSpell;
	if (inString == "duration") return jsbuff::eDuration;
	if (inString == "initialDuration") return jsbuff::eInitialDuration;
	if (inString == "hitCount") return jsbuff::eHitCount;
	if (inString == "modifier") return jsbuff::eModifier;
	if (inString == "x") return jsbuff::eX;
	if (inString == "y") return jsbuff::eY;
	if (inString == "z") return jsbuff::eZ;
	if (inString == "type") return jsbuff::eType;
	if (inString == "casterLevel") return jsbuff::eCasterLevel;
}

void GetBuffAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info) {

	v8::Local<v8::External> ext = info.Data().As<v8::External>();

	PSPELLBUFF buff_info = static_cast<PSPELLBUFF>(ext->Value());

	auto isolate = info.GetIsolate();
	v8::Local<v8::Value> val = cast_to_js<const char*>(info, "Undefined");
	v8::String::Utf8Value accessor(info.GetIsolate(), name);

	switch (hashit(std::string(*accessor))) {
	case jsbuff::eSpellId:
		val = cast_to_js(info, buff_info->SpellID);
		break;
	case jsbuff::eSpell:
		val = GetSpell(GetSpellByID(buff_info->SpellID), info);
		break;
	case jsbuff::eDuration:
		val = cast_to_js(info, buff_info->Duration);
		break;
	case jsbuff::eInitialDuration:
		val = cast_to_js(info, buff_info->InitialDuration);
		break;
	case jsbuff::eHitCount:
		val = cast_to_js(info, buff_info->HitCount);
		break;
	case jsbuff::eModifier:
		val = cast_to_js(info, buff_info->Modifier);
		break;
	case jsbuff::eX:
		val = cast_to_js(info, buff_info->X);
		break;
	case jsbuff::eY:
		val = cast_to_js(info, buff_info->Y);
		break;
	case jsbuff::eZ:
		val = cast_to_js(info, buff_info->Z);
		break;
	case jsbuff::eType:
		val = cast_to_js(info, buff_info->Type);
		break;
	case jsbuff::eCasterLevel:
		val = cast_to_js(info, buff_info->Level);
		break;
	}

	info.GetReturnValue().Set(val);

}

v8::Local<v8::Value> GetBuff(PSPELLBUFF spell_buff, const v8::PropertyCallbackInfo<Value>& info) {
	auto isolate = info.GetIsolate();
	auto context = isolate->GetCurrentContext();
	auto obj = v8::Object::New(isolate);
	auto ext = v8::External::New(isolate, spell_buff);

	std::function<void(const char*)> set_accessor =
		[context, obj, isolate, ext](
			const char* accessor) -> void {
		obj->SetAccessor(context,
			GetLocalName(isolate, accessor),
			GetBuffAttribute, nullptr, ext);
	};

	std::vector<std::string> attribute_list{
		"spellId","spell","duration","initialDuration","hitCount","modifier","x","y","z","type","casterLevel"
	};
	for (std::string attribute : attribute_list) {
		set_accessor(attribute.c_str());
	}
	return obj;
}
