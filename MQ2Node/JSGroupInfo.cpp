#include "JSGroupInfo.h"
#include "Global.h"

// hash fn
// r.split(',')
//    .map(s = > s.trim())
//    .map(s = > `jsspawn::$ { s }`)
//    .map(s = >
//             {
//               const v = s.replace('jsspawn::', '');
//               return `if (inString == "${v.slice(1).charAt(0).toLowerCase() +
//               "
//                                       "v.slice(2)}") return ${s};
//               `;
//             })
//    .join('\r\n');

// Case fn
//r.split(',').map(s = > s.trim()).map(s = > `case jsgroup::${ s }:
//val = cast_to_js(info, spawn_info->);
//break; `).join('\r\n')
//
//// For array init
//r.split('\n').map(a = > a.trim().replace(',', '')).map(s = > `${s.slice(1).charAt(0).toLowerCase() + s.slice(2)}`).map(s = > `"${s}"`).join(',')


void GetGroupInfoAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info);

void GetGroupMemberAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info);

namespace jsgroup {
	enum string_code {
		eMember,
		eLeader,
		eName,
		eOwnerName,
		eLevel,
		eOffline,
		eMainTank,
		eMainAssist,
		ePuller,
		eMarkNpc,
		eMasterLooter,
		eRoles,
		espawn
	};
}

jsgroup::string_code hashit(std::string const& inString) {
	if (inString == "member") return jsgroup::eMember;
	if (inString == "leader") return jsgroup::eLeader;
	if (inString == "name") return jsgroup::eName;
	if (inString == "ownerName") return jsgroup::eOwnerName;
	if (inString == "level") return jsgroup::eLevel;
	if (inString == "offline") return jsgroup::eOffline;
	if (inString == "mainTank") return jsgroup::eMainTank;
	if (inString == "mainAssist") return jsgroup::eMainAssist;
	if (inString == "puller") return jsgroup::ePuller;
	if (inString == "markNpc") return jsgroup::eMarkNpc;
	if (inString == "masterLooter") return jsgroup::eMasterLooter;
	if (inString == "roles") return jsgroup::eRoles;
	if (inString == "spawn") return jsgroup::espawn;
}

v8::Local<v8::Array> GetGroupMembers(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info) {

	v8::Local<v8::External> ext = info.Data().As<v8::External>();
	GROUPINFO* group_info = static_cast<GROUPINFO*>(ext->Value());
	auto isolate = info.GetIsolate();
	auto arr = v8::Array::New(isolate);
	for (int i = 0; i < 6; i++) {
		arr->Set(isolate->GetCurrentContext(), i, GetSpawn(GetGroupMember(i), info.GetIsolate()));
	}
	return arr;
}

v8::Local<v8::Object> GetGroupInfo(GROUPINFO* group_info, const v8::PropertyCallbackInfo<Value>& info) {
	auto isolate = info.GetIsolate();
	auto context = isolate->GetCurrentContext();

	auto obj = v8::Object::New(isolate);
	auto ext = v8::External::New(isolate, group_info);

	std::function<void(const char*)> set_accessor =
		[context, obj, isolate, ext](
			const char* accessor) -> void {
		obj->SetAccessor(context,
			GetLocalName(isolate, accessor),
			GetGroupInfoAttribute, nullptr, ext);
	};

	std::vector<std::string> attribute_list{ "leader",
											"member",
	};
	for (std::string attribute : attribute_list) {
		set_accessor(attribute.c_str());
	}
	return obj;
}

v8::Local<v8::Value> GetGroupMember(GROUPMEMBER* group_info, const v8::PropertyCallbackInfo<Value>& info) {
	auto isolate = info.GetIsolate();
	auto context = isolate->GetCurrentContext();
	auto obj = v8::Object::New(isolate);
	v8::Local<v8::External> ext = info.Data().As<v8::External>();

	GROUPINFO* group_member = static_cast<GROUPINFO*>(ext->Value());

	if (group_member == nullptr) {
		return v8::Null(isolate);

	}
	std::function<void(const char*)> set_accessor =
		[context, obj, isolate, ext](
			const char* accessor) -> void {
		obj->SetAccessor(context,
			GetLocalName(isolate, accessor),
			GetGroupMemberAttribute, nullptr, ext);
	};

	std::vector<std::string> attribute_list{ "member", "leader", "name", "ownerName", "level", "offline", "mainTank", "mainAssist", "puller", "markNpc", "masterLooter", "roles", "spawn",
	};
	for (std::string attribute : attribute_list) {
		set_accessor(attribute.c_str());
	}
}

void GetGroupInfoAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info) {

	v8::Local<v8::External> ext = info.Data().As<v8::External>();
	GROUPINFO* group_info = static_cast<GROUPINFO*>(ext->Value());
	auto isolate = info.GetIsolate();
	v8::Local<v8::Value> val = cast_to_js<const char*>(info, "Undefined");
	v8::String::Utf8Value accessor(info.GetIsolate(), name);

	switch (hashit(std::string(*accessor))) {
	case jsgroup::eMember:
		val = GetGroupMembers(name, info);
		break;
	case jsgroup::eLeader:
		val = cast_to_js(info, group_info->pLeader);
		break;
	}

	info.GetReturnValue().Set(val);

}

void GetGroupMemberAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info) {

	v8::Local<v8::External> ext = info.Data().As<v8::External>();
	GROUPMEMBER* group_member = static_cast<GROUPMEMBER*>(ext->Value());
	auto isolate = info.GetIsolate();
	v8::Local<v8::Value> val = cast_to_js<const char*>(info, "Undefined");
	v8::String::Utf8Value accessor(info.GetIsolate(), name);

	char szTemp[MAX_STRING * 8] = { 0 };

	switch (hashit(std::string(*accessor))) {
	case jsgroup::eName:
		GetCXStr(group_member->pName, szTemp, MAX_STRING * 8);
		val = cast_to_js(info, szTemp);
		break;
	case jsgroup::eOwnerName:
		GetCXStr(group_member->pOwner, szTemp, MAX_STRING * 8);
		val = cast_to_js(info, szTemp);
		break;
	case jsgroup::eLevel:
		val = cast_to_js(info, group_member->Level);
		break;
	case jsgroup::eOffline:
		val = cast_to_js(info, group_member->Offline);
		break;
	case jsgroup::eMainTank:
		val = cast_to_js(info, group_member->MainTank);
		break;
	case jsgroup::eMainAssist:
		val = cast_to_js(info, group_member->MainAssist);
		break;
	case jsgroup::ePuller:
		val = cast_to_js(info, group_member->Puller);
		break;
	case jsgroup::eMarkNpc:
		val = cast_to_js(info, group_member->MarkNpc);
		break;
	case jsgroup::eMasterLooter:
		val = cast_to_js(info, group_member->MasterLooter);
		break;
	case jsgroup::eRoles:
		val = cast_to_js(info, group_member->Roles);
		break;
	case jsgroup::espawn:
		val = cast_to_js(info, group_member->pSpawn);
		break;
	}

	info.GetReturnValue().Set(val);
}
