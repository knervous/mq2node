#include "Global.h"
#include "JSXTarget.h"
// hash fn
// r.split(',')
//    .map(s = > s.trim())
//    .map(s = > `jsspawn::$ { s }`)
//    .map(s = >
//             {
//               const v = s.replace('jsspawn::', '');
//               return `if (inString == "${v.slice(1).charAt(0).toLowerCase() +
//               "
//                                       "v.slice(2)}") return ${s};
//               `;
//             })
//    .join('\r\n');

// Case fn
// r.split(',').map(s =>s.trim()).map(s => `case jsspawn::${s}:
// val = cast_to_js(info, spawn_info->);
// break;`).join('\r\n')

// For array init
// r.split('\n').map(a =>
// a.trim().replace(',','')).map(s=>`${s.slice(1).charAt(0).toLowerCase() +
// s.slice(2)}`).map(s =>`"${s}"`).join(',')

namespace jsspawn {

enum string_code {
  ePrev,
  eNext,
  eSpeedMultiplier,
  eTimeStamp,
  eLastName,
  eX,
  eY,
  eZ,
  eSpeedX,
  eSpeedY,
  eSpeedZ,
  eSpeedRun,
  eHeading,
  eSpeedHeading,
  eCameraAngle,
  eUnderwater,
  eFeetWet,
  eName,
  eDisplayedName,
  eType,
  eBodyType,
  eAvatarHeight,
  eAvatarHeight2,
  eSpawnId,
  eIsABoat,
  eMount,
  eRider,
  eEnduranceCurrent,
  eMercenary,
  eZone,
  eInstance,
  eLight,
  eGuildStatus,
  eLastTick,
  eLevel,
  eGM,
  eFishingEta,
  eSneak,
  ePetId,
  eAnon,
  eRespawnTimer,
  eAARank,
  eHpMax,
  eHpCurrent,
  eInnateEta,
  eGetMeleeRangeVar1,
  eTrader,
  eHolding,
  eHideMode,
  ePvpFlag,
  eGuildId,
  eRunSpeed,
  eManaMax,
  eStandState,
  eMasterId,
  eAfk,
  eEnduranceMax,
  eLinkdead,
  eBuyer,
  eTitle,
  eFishingEvent,
  eSuffix,
  eLfg,
  eCastingData,
  eManaCurrent,
  eDeity,
  eWhoFollowing,
  eGroupAssistNpc,
  eRaidAssistNpc,
  eGroupMarkNpc,
  eRaidMarkNpc,
  eTargetOfTarget,
  eRace,
  eClass,
  eGender,
  eActorDef,
  eArmorColor,
  eEquipment,
  eGetMeleeRangeVar2,
  eAnimation,
  eWalkSpeed,
  eHideCorpse,
  eInvitedToGroup,
  eGroupMemberTargeted,
  eLevitate
};
}

jsspawn::string_code hashit(std::string const& inString) {
  if (inString == "prev") return jsspawn::ePrev;
  if (inString == "next") return jsspawn::eNext;
  if (inString == "speedMultiplier") return jsspawn::eSpeedMultiplier;
  if (inString == "timeStamp") return jsspawn::eTimeStamp;
  if (inString == "lastName") return jsspawn::eLastName;
  if (inString == "x") return jsspawn::eX;
  if (inString == "y") return jsspawn::eY;
  if (inString == "z") return jsspawn::eZ;
  if (inString == "speedX") return jsspawn::eSpeedX;
  if (inString == "speedY") return jsspawn::eSpeedY;
  if (inString == "speedZ") return jsspawn::eSpeedZ;
  if (inString == "speedRun") return jsspawn::eSpeedRun;
  if (inString == "heading") return jsspawn::eHeading;
  if (inString == "speedHeading") return jsspawn::eSpeedHeading;
  if (inString == "cameraAngle") return jsspawn::eCameraAngle;
  if (inString == "underwater") return jsspawn::eUnderwater;
  if (inString == "feetWet") return jsspawn::eFeetWet;
  if (inString == "name") return jsspawn::eName;
  if (inString == "displayedName") return jsspawn::eDisplayedName;
  if (inString == "type") return jsspawn::eType;
  if (inString == "bodyType") return jsspawn::eBodyType;
  if (inString == "avatarHeight") return jsspawn::eAvatarHeight;
  if (inString == "avatarHeight2") return jsspawn::eAvatarHeight2;
  if (inString == "spawnId") return jsspawn::eSpawnId;
  if (inString == "isABoat") return jsspawn::eIsABoat;
  if (inString == "mount") return jsspawn::eMount;
  if (inString == "rider") return jsspawn::eRider;
  if (inString == "enduranceCurrent") return jsspawn::eEnduranceCurrent;
  if (inString == "mercenary") return jsspawn::eMercenary;
  if (inString == "zone") return jsspawn::eZone;
  if (inString == "instance") return jsspawn::eInstance;
  if (inString == "light") return jsspawn::eLight;
  if (inString == "guildStatus") return jsspawn::eGuildStatus;
  if (inString == "lastTick") return jsspawn::eLastTick;
  if (inString == "level") return jsspawn::eLevel;
  if (inString == "gM") return jsspawn::eGM;
  if (inString == "fishingEta") return jsspawn::eFishingEta;
  if (inString == "sneak") return jsspawn::eSneak;
  if (inString == "petId") return jsspawn::ePetId;
  if (inString == "anon") return jsspawn::eAnon;
  if (inString == "respawnTimer") return jsspawn::eRespawnTimer;
  if (inString == "aARank") return jsspawn::eAARank;
  if (inString == "hpMax") return jsspawn::eHpMax;
  if (inString == "hpCurrent") return jsspawn::eHpCurrent;
  if (inString == "innateEta") return jsspawn::eInnateEta;
  if (inString == "getMeleeRangeVar1") return jsspawn::eGetMeleeRangeVar1;
  if (inString == "trader") return jsspawn::eTrader;
  if (inString == "holding") return jsspawn::eHolding;
  if (inString == "hideMode") return jsspawn::eHideMode;
  if (inString == "pvpFlag") return jsspawn::ePvpFlag;
  if (inString == "guildId") return jsspawn::eGuildId;
  if (inString == "runSpeed") return jsspawn::eRunSpeed;
  if (inString == "manaMax") return jsspawn::eManaMax;
  if (inString == "standState") return jsspawn::eStandState;
  if (inString == "masterId") return jsspawn::eMasterId;
  if (inString == "afk") return jsspawn::eAfk;
  if (inString == "enduranceMax") return jsspawn::eEnduranceMax;
  if (inString == "linkdead") return jsspawn::eLinkdead;
  if (inString == "buyer") return jsspawn::eBuyer;
  if (inString == "title") return jsspawn::eTitle;
  if (inString == "fishingEvent") return jsspawn::eFishingEvent;
  if (inString == "suffix") return jsspawn::eSuffix;
  if (inString == "lfg") return jsspawn::eLfg;
  if (inString == "castingData") return jsspawn::eCastingData;
  if (inString == "manaCurrent") return jsspawn::eManaCurrent;
  if (inString == "deity") return jsspawn::eDeity;
  if (inString == "whoFollowing") return jsspawn::eWhoFollowing;
  if (inString == "groupAssistNpc") return jsspawn::eGroupAssistNpc;
  if (inString == "raidAssistNpc") return jsspawn::eRaidAssistNpc;
  if (inString == "groupMarkNpc") return jsspawn::eGroupMarkNpc;
  if (inString == "raidMarkNpc") return jsspawn::eRaidMarkNpc;
  if (inString == "targetOfTarget") return jsspawn::eTargetOfTarget;
  if (inString == "race") return jsspawn::eRace;
  if (inString == "class") return jsspawn::eClass;
  if (inString == "gender") return jsspawn::eGender;
  if (inString == "actorDef") return jsspawn::eActorDef;
  if (inString == "armorColor") return jsspawn::eArmorColor;
  if (inString == "equipment") return jsspawn::eEquipment;
  if (inString == "getMeleeRangeVar2") return jsspawn::eGetMeleeRangeVar2;
  if (inString == "animation") return jsspawn::eAnimation;
  if (inString == "walkSpeed") return jsspawn::eWalkSpeed;
  if (inString == "hideCorpse") return jsspawn::eHideCorpse;
  if (inString == "invitedToGroup") return jsspawn::eInvitedToGroup;
  if (inString == "groupMemberTargeted") return jsspawn::eGroupMemberTargeted;
  if (inString == "levitate") return jsspawn::eLevitate;
}

void GetTargetAttribute(v8::Local<v8::Name> name,
                        const v8::PropertyCallbackInfo<v8::Value>& info) {
  v8::Local<v8::External> ext = info.Data().As<v8::External>();
  _SPAWNINFO* spawn_info = static_cast<_SPAWNINFO*>(ext->Value());
  auto isolate = info.GetIsolate();
  v8::Local<v8::Value> val = cast_to_js<const char*>(info, "Undefined");
  v8::String::Utf8Value accessor(info.GetIsolate(), name);

  switch (hashit(std::string(*accessor))) {
    case jsspawn::ePrev:
      val = cast_to_js(info, spawn_info->pPrev);
      break;
    case jsspawn::eNext:
      val = cast_to_js(info, spawn_info->pNext);
      break;
    case jsspawn::eSpeedMultiplier:
      val = cast_to_js(info, spawn_info->SpeedMultiplier);
      break;
    case jsspawn::eTimeStamp:
      val = cast_to_js(info, spawn_info->TimeStamp);
      break;
    case jsspawn::eLastName:
      val = cast_to_js(info, spawn_info->Lastname);
      break;
    case jsspawn::eX:
      val = cast_to_js(info, spawn_info->X);
      break;
    case jsspawn::eY:
      val = cast_to_js(info, spawn_info->Y);
      break;
    case jsspawn::eZ:
      val = cast_to_js(info, spawn_info->Z);
      break;
    case jsspawn::eSpeedX:
      val = cast_to_js(info, spawn_info->SpeedX);
      break;
    case jsspawn::eSpeedY:
      val = cast_to_js(info, spawn_info->SpeedY);
      break;
    case jsspawn::eSpeedZ:
      val = cast_to_js(info, spawn_info->SpeedZ);
      break;
    case jsspawn::eSpeedRun:
      val = cast_to_js(info, spawn_info->SpeedRun);
      break;
    case jsspawn::eHeading:
      val = cast_to_js(info, spawn_info->Heading);
      break;
    case jsspawn::eSpeedHeading:
      val = cast_to_js(info, spawn_info->SpeedHeading);
      break;
    case jsspawn::eCameraAngle:
      val = cast_to_js(info, spawn_info->CameraAngle);
      break;
    case jsspawn::eUnderwater:
      val = cast_to_js(info, spawn_info->UnderWater);
      break;
    case jsspawn::eFeetWet:
      val = cast_to_js(info, spawn_info->FeetWet);
      break;
    case jsspawn::eName:
      val = cast_to_js(info, spawn_info->Name);
      break;
    case jsspawn::eDisplayedName:
      val = cast_to_js(info, spawn_info->DisplayedName);
      break;
    case jsspawn::eType:
      val = cast_to_js(info, spawn_info->Type);
      break;
    case jsspawn::eBodyType:
      break;
    case jsspawn::eAvatarHeight:
      val = cast_to_js(info, spawn_info->AvatarHeight);
      break;

    case jsspawn::eSpawnId:
      val = cast_to_js(info, spawn_info->SpawnID);
      break;

    case jsspawn::eMount:
      // val = cast_to_js(info, spawn_info->m);
      break;
    case jsspawn::eRider:
      // val = cast_to_js(info, spawn_info->);
      break;
    case jsspawn::eEnduranceCurrent:
      val = cast_to_js(info, spawn_info->EnduranceCurrent);
      break;
    case jsspawn::eMercenary:
      val = cast_to_js(info, spawn_info->Mercenary);
      break;
    case jsspawn::eZone:
      val = cast_to_js(info, spawn_info->Zone);
      break;

    case jsspawn::eLight:
      val = cast_to_js(info, spawn_info->Light);
      break;

    case jsspawn::eLastTick:
      val = cast_to_js(info, spawn_info->LastTick);
      break;
    case jsspawn::eLevel:
      val = cast_to_js(info, spawn_info->Level);
      break;
    case jsspawn::eGM:
      val = cast_to_js(info, spawn_info->GM);
      break;
    case jsspawn::eFishingEta:
      val = cast_to_js(info, spawn_info->FishingETA);
      break;
    case jsspawn::eSneak:
      val = cast_to_js(info, spawn_info->Sneak);
      break;
    case jsspawn::ePetId:
      val = cast_to_js(info, spawn_info->PetID);
      break;
    case jsspawn::eAnon:
      val = cast_to_js(info, spawn_info->Anon);
      break;
    case jsspawn::eRespawnTimer:
      val = cast_to_js(info, spawn_info->RespawnTimer);
      break;
    case jsspawn::eAARank:
      val = cast_to_js(info, spawn_info->AARank);
      break;
    case jsspawn::eHpMax:
      val = cast_to_js(info, spawn_info->HPMax);
      break;
    case jsspawn::eHpCurrent:
      val = cast_to_js(info, spawn_info->HPCurrent);
      break;

    case jsspawn::eGetMeleeRangeVar1:
      val = cast_to_js(info, spawn_info->GetMeleeRangeVar1);
      break;
    case jsspawn::eTrader:
      val = cast_to_js(info, spawn_info->Trader);
      break;

    case jsspawn::eHideMode:
      val = cast_to_js(info, spawn_info->HideMode);
      break;
    case jsspawn::ePvpFlag:
      val = cast_to_js(info, spawn_info->PvPFlag);
      break;
    case jsspawn::eGuildId:
      val = cast_to_js(info, spawn_info->GuildID);
      break;
    case jsspawn::eRunSpeed:
      val = cast_to_js(info, spawn_info->RunSpeed);
      break;
    case jsspawn::eManaMax:
      val = cast_to_js(info, spawn_info->ManaMax);
      break;
    case jsspawn::eStandState:
      val = cast_to_js(info, spawn_info->StandState);
      break;
    case jsspawn::eMasterId:
      val = cast_to_js(info, spawn_info->MasterID);
      break;
    case jsspawn::eAfk:
      val = cast_to_js(info, spawn_info->AFK);
      break;
    case jsspawn::eEnduranceMax:
      val = cast_to_js(info, spawn_info->EnduranceMax);
      break;
    case jsspawn::eLinkdead:
      val = cast_to_js(info, spawn_info->Linkdead);
      break;
    case jsspawn::eBuyer:
      val = cast_to_js(info, spawn_info->Buyer);
      break;
    case jsspawn::eTitle:
      val = cast_to_js(info, spawn_info->Title);
      break;
    case jsspawn::eFishingEvent:
      val = cast_to_js(info, spawn_info->FishingEvent);
      break;
    case jsspawn::eSuffix:
      val = cast_to_js(info, spawn_info->Suffix);
      break;
    case jsspawn::eLfg:
      val = cast_to_js(info, spawn_info->LFG);
      break;
    case jsspawn::eCastingData:
      // TODO pcj
      // val = cast_to_js(info, spawn_info->CastingData);
      break;
    case jsspawn::eManaCurrent:
      val = cast_to_js(info, spawn_info->ManaCurrent);
      break;
    case jsspawn::eDeity:
      val = cast_to_js(info, spawn_info->Deity);
      break;
    case jsspawn::eWhoFollowing:
      val = cast_to_js(info, spawn_info->WhoFollowing);
      break;
    case jsspawn::eGroupAssistNpc:
      val = cast_to_js(info, spawn_info->GroupAssistNPC);
      break;
    case jsspawn::eRaidAssistNpc:
      val = cast_to_js(info, spawn_info->RaidAssistNPC);
      break;
    case jsspawn::eGroupMarkNpc:
      val = cast_to_js(info, spawn_info->GroupMarkNPC);
      break;
    case jsspawn::eRaidMarkNpc:
      val = cast_to_js(info, spawn_info->RaidMarkNPC);
      break;
    case jsspawn::eTargetOfTarget:
      val = cast_to_js(info, spawn_info->Targetable);
      break;
#ifdef ROF2
    case jsspawn::eAvatarHeight2:
      val = cast_to_js(info, spawn_info->AvatarHeight2);
      break;
    case jsspawn::eIsABoat:
      val = cast_to_js(info, spawn_info->IsABoat);
      break;
    case jsspawn::eInstance:
      val = cast_to_js(info, spawn_info->Instance);
      break;
    case jsspawn::eGuildStatus:
      val = cast_to_js(info, spawn_info->GuildStatus);
      break;
    case jsspawn::eInnateEta:
      val = cast_to_js(info, spawn_info->InnateETA);
      break;
    case jsspawn::eHolding:
      val = cast_to_js(info, spawn_info->Holding);
      break;
    case jsspawn::eRace:
      val = cast_to_js(info, spawn_info->Race);
      break;
    case jsspawn::eClass:
      val = cast_to_js(info, spawn_info->Class);
      break;
    case jsspawn::eGender:
      val = cast_to_js(info, spawn_info->Gender);
      break;
    case jsspawn::eActorDef:
      val = cast_to_js(info, spawn_info->ActorDef);
      break;
    case jsspawn::eArmorColor:
      val = cast_to_js(info, spawn_info->ArmorColor);
      break;
    case jsspawn::eHideCorpse:
      val = cast_to_js(info, spawn_info->HideCorpse);
      break;
    case jsspawn::eLevitate:
      val = cast_to_js(info, spawn_info->Levitate);
      break;
    case jsspawn::eGetMeleeRangeVar2:
      val = cast_to_js(info, spawn_info->GetMeleeRangeVar2);
      break;
#else
    case jsspawn::eRace:
      val = cast_to_js(info, spawn_info->GetRace());
      break;
    case jsspawn::eClass:
      val = cast_to_js(info, spawn_info->GetClass());
      break;
#endif
    case jsspawn::eEquipment:
      // TODO pcj
      // val = cast_to_js(info, spawn_info->Equi);
      break;

    case jsspawn::eAnimation:
      val = cast_to_js(info, spawn_info->Animation);
      break;
    case jsspawn::eWalkSpeed:
      val = cast_to_js(info, spawn_info->WalkSpeed);
      break;

    case jsspawn::eInvitedToGroup:
      val = cast_to_js(info, spawn_info->InvitedToGroup);
      break;
    case jsspawn::eGroupMemberTargeted:
      val = cast_to_js(info, spawn_info->GroupMemberTargeted);
      break;

    default:
      val = cast_to_js(info, "Undefined");
      break;
  }
  info.GetReturnValue().Set(val);
}

v8::Local<v8::Value> GetSpawn(_SPAWNINFO* spawn_info,
                              Isolate * isolate) {
  auto context = isolate->GetCurrentContext();
  auto obj = v8::Object::New(isolate);
  auto ext = v8::External::New(isolate, spawn_info);

  std::function<void(const char*)> set_accessor =
      [context, obj, isolate, ext](
          const char* accessor) -> void {
    obj->SetAccessor(context,
                     GetLocalName(isolate, accessor),
                     GetTargetAttribute, nullptr, ext);
  };

  // NULL (0) is used as a possible value from some methods
  if (spawn_info != nullptr && spawn_info != NULL) {
    std::vector<std::string> attribute_list{"prev",
                                            "next",
                                            "speedMultiplier",
                                            "timeStamp",
                                            "lastName",
                                            "x",
                                            "y",
                                            "z",
                                            "speedX",
                                            "speedY",
                                            "speedZ",
                                            "speedRun",
                                            "heading",
                                            "speedHeading",
                                            "cameraAngle",
                                            "underwater",
                                            "feetWet",
                                            "name",
                                            "displayedName",
                                            "type",
                                            "bodyType",
                                            "avatarHeight",
                                            "avatarHeight2",
                                            "spawnId",
                                            "isABoat",
                                            "mount",
                                            "rider",
                                            "enduranceCurrent",
                                            "mercenary",
                                            "zone",
                                            "instance",
                                            "light",
                                            "guildStatus",
                                            "lastTick",
                                            "level",
                                            "gM",
                                            "fishingEta",
                                            "sneak",
                                            "petId",
                                            "anon",
                                            "respawnTimer",
                                            "aARank",
                                            "hpMax",
                                            "hpCurrent",
                                            "innateEta",
                                            "getMeleeRangeVar1",
                                            "trader",
                                            "holding",
                                            "hideMode",
                                            "pvpFlag",
                                            "guildId",
                                            "runSpeed",
                                            "manaMax",
                                            "standState",
                                            "masterId",
                                            "afk",
                                            "enduranceMax",
                                            "linkdead",
                                            "buyer",
                                            "title",
                                            "fishingEvent",
                                            "suffix",
                                            "lfg",
                                            "castingData",
                                            "manaCurrent",
                                            "deity",
                                            "whoFollowing",
                                            "groupAssistNpc",
                                            "raidAssistNpc",
                                            "groupMarkNpc",
                                            "raidMarkNpc",
                                            "targetOfTarget",
                                            "race",
                                            "class",
                                            "gender",
                                            "actorDef",
                                            "armorColor",
                                            "equipment",
                                            "getMeleeRangeVar2",
                                            "animation",
                                            "walkSpeed",
                                            "hideCorpse",
                                            "invitedToGroup",
                                            "groupMemberTargeted",
                                            "levitate"};
    for (std::string attribute : attribute_list) {
      set_accessor(attribute.c_str());
    }
  } else {
    return v8::Null(isolate);
  }
  return obj;
}
