#pragma once

#define NODE_WANT_INTERNALS true

#include <assert.h>
#include "node.h"
#include "node_errors.h"
#include "node_internals.h"
#include "v8-util.h"
#include "uv.h"

#include "MQ2Plugin.h"

using node::ArrayBufferAllocator;
using node::Environment;
using node::IsolateData;
using node::MultiIsolatePlatform;
using node::errors::TryCatchScope;
using v8::Context;
using v8::HandleScope;
using v8::Isolate;
using v8::Local;
using v8::Locker;
using v8::Unlocker;
using v8::MaybeLocal;
using v8::SealHandleScope;
using v8::String;
using v8::V8;
using v8::Value;

struct payload {
	std::string* type;
	void* data;
};

class NodeRuntime {
	
public:
	NodeRuntime(){};
	~NodeRuntime() = default;

	void Eval(bool module_load = false,
		const char* module_path = nullptr);
	void ModuleLoad(const char* module_path);
	void Init();
	void Dispose();
	void StopExecution();
	void SetRunningScript(std::string eval);

	const bool GetIsRunning() { return script_running_;  };

	// MQ events
	void OnSpawnEvent(PSPAWNINFO spawn, const char* type);
	void OnZoneEvent(const char * type);
	void OnChatEvent(const char* line);

private:
	std::unique_ptr<MultiIsolatePlatform> platform_;
	std::shared_ptr<ArrayBufferAllocator> allocator_;
	std::function<void()> halt_exec_;

	std::string running_script_;
	std::string module_path_;
	std::string exec_path_;


	std::vector<std::function<void(Isolate*isolate, v8::Local<v8::Context> context)>> pending_tasks_;


	bool allow_execution_ = true;
	bool module_load_ = false;
	bool debug_ = false;
	bool script_running_ = false;
};
