#include "JSGroupInfo.h"
#include "Global.h"

// hash fn
// r.split(',')
//    .map(s = > s.trim())
//    .map(s = > `jsspawn::$ { s }`)
//    .map(s = >
//             {
//               const v = s.replace('jsspawn::', '');
//               return `if (inString == "${v.slice(1).charAt(0).toLowerCase() +
//               "
//                                       "v.slice(2)}") return ${s};
//               `;
//             })
//    .join('\r\n');

// Case fn
//r.split(',').map(s = > s.trim()).map(s = > `case jsgroup::${ s }:
//val = cast_to_js(info, spawn_info->);
//break; `).join('\r\n')
//
//// For array init
//r.split('\n').map(a = > a.trim().replace(',', '')).map(s = > `${s.slice(1).charAt(0).toLowerCase() + s.slice(2)}`).map(s = > `"${s}"`).join(',')


namespace jsspell {
	enum string_code {
		eRange,
		eAERange,
		ePushBack,
		ePushUp,
		eCastTime,
		eRecoveryTime,
		eRecastTime,
		eDurationType,
		eDurationCap,
		eManaCost,
		eReagentID,
		eReagentCount,
		eNoExpendReagent,
		eCalcIndex,
		eNumEffects,
		eBookIcon,
		eGemIcon,
		eDescriptionIndex,
		eResistAdj,
		eDeity,
		eSpellAnim,
		eSpellIcon,
		eDurationParticleEffect,
		eId,
		eHateMod,
		eResistPerLevel,
		eResistCap,
		eEnduranceCost,
		eEnduranceValue,
		eHateGenerated,
		eHitCountType,
		eHitCount,
		eConeStartAngle,
		eConeEndAngle,
		ePvpResistBase,
		ePvpCalc,
		ePvpResistCap,
		ePvpDuration,
		eSpellGroup,
		eSpellSubGroup,
		eSpellRank,
		eMaxResist,
		eMinResist,
		eSpreadRadius,
		eCritChanceOverride,
		eMaxTargets,
		eMinRange,
		eUninterruptable,
		eNotStackableDot,
		eDeletable,
		eBypassRegenCheck,
		eCanCastInCombat,
		eCanCastOutOfCombat,
		eCastNotStanding,
		eCanMgb,
		eNoDispell,
		eIsSkill,
		eStacksWithDiscs,
		eSpellType,
		eResist,
		eTargetType,
		eCastDifficulty,
		eSkill,
		eZoneType,
		eEnvironment,
		eTimeOfDay,
		eCastingAnim,
		eCancelOnSit,
		eName,
		eTarget,
		eNoRemove,
		eDescription

	};
}


jsspell::string_code hashit(std::string const& inString) {
	if (inString == "range") return jsspell::eRange;
	if (inString == "aERange") return jsspell::eAERange;
	if (inString == "pushBack") return jsspell::ePushBack;
	if (inString == "pushUp") return jsspell::ePushUp;
	if (inString == "castTime") return jsspell::eCastTime;
	if (inString == "recoveryTime") return jsspell::eRecoveryTime;
	if (inString == "recastTime") return jsspell::eRecastTime;
	if (inString == "durationType") return jsspell::eDurationType;
	if (inString == "durationCap") return jsspell::eDurationCap;
	if (inString == "manaCost") return jsspell::eManaCost;
	if (inString == "reagentID") return jsspell::eReagentID;
	if (inString == "reagentCount") return jsspell::eReagentCount;
	if (inString == "noExpendReagent") return jsspell::eNoExpendReagent;
	if (inString == "calcIndex") return jsspell::eCalcIndex;
	if (inString == "numEffects") return jsspell::eNumEffects;
	if (inString == "bookIcon") return jsspell::eBookIcon;
	if (inString == "gemIcon") return jsspell::eGemIcon;
	if (inString == "descriptionIndex") return jsspell::eDescriptionIndex;
	if (inString == "resistAdj") return jsspell::eResistAdj;
	if (inString == "deity") return jsspell::eDeity;
	if (inString == "spellAnim") return jsspell::eSpellAnim;
	if (inString == "spellIcon") return jsspell::eSpellIcon;
	if (inString == "durationParticleEffect") return jsspell::eDurationParticleEffect;
	if (inString == "id") return jsspell::eId;
	if (inString == "hateMod") return jsspell::eHateMod;
	if (inString == "resistPerLevel") return jsspell::eResistPerLevel;
	if (inString == "resistCap") return jsspell::eResistCap;
	if (inString == "enduranceCost") return jsspell::eEnduranceCost;
	if (inString == "enduranceValue") return jsspell::eEnduranceValue;
	if (inString == "hateGenerated") return jsspell::eHateGenerated;
	if (inString == "hitCountType") return jsspell::eHitCountType;
	if (inString == "hitCount") return jsspell::eHitCount;
	if (inString == "coneStartAngle") return jsspell::eConeStartAngle;
	if (inString == "coneEndAngle") return jsspell::eConeEndAngle;
	if (inString == "pvpResistBase") return jsspell::ePvpResistBase;
	if (inString == "pvpCalc") return jsspell::ePvpCalc;
	if (inString == "pvpResistCap") return jsspell::ePvpResistCap;
	if (inString == "pvpDuration") return jsspell::ePvpDuration;
	if (inString == "spellGroup") return jsspell::eSpellGroup;
	if (inString == "spellSubGroup") return jsspell::eSpellSubGroup;
	if (inString == "spellRank") return jsspell::eSpellRank;
	if (inString == "maxResist") return jsspell::eMaxResist;
	if (inString == "minResist") return jsspell::eMinResist;
	if (inString == "spreadRadius") return jsspell::eSpreadRadius;
	if (inString == "critChanceOverride") return jsspell::eCritChanceOverride;
	if (inString == "maxTargets") return jsspell::eMaxTargets;
	if (inString == "minRange") return jsspell::eMinRange;
	if (inString == "uninterruptable") return jsspell::eUninterruptable;
	if (inString == "notStackableDot") return jsspell::eNotStackableDot;
	if (inString == "deletable") return jsspell::eDeletable;
	if (inString == "bypassRegenCheck") return jsspell::eBypassRegenCheck;
	if (inString == "canCastInCombat") return jsspell::eCanCastInCombat;
	if (inString == "canCastOutOfCombat") return jsspell::eCanCastOutOfCombat;
	if (inString == "castNotStanding") return jsspell::eCastNotStanding;
	if (inString == "canMgb") return jsspell::eCanMgb;
	if (inString == "noDispell") return jsspell::eNoDispell;
	if (inString == "isSkill") return jsspell::eIsSkill;
	if (inString == "stacksWithDiscs") return jsspell::eStacksWithDiscs;
	if (inString == "spellType") return jsspell::eSpellType;
	if (inString == "resist") return jsspell::eResist;
	if (inString == "targetType") return jsspell::eTargetType;
	if (inString == "castDifficulty") return jsspell::eCastDifficulty;
	if (inString == "skill") return jsspell::eSkill;
	if (inString == "zoneType") return jsspell::eZoneType;
	if (inString == "environment") return jsspell::eEnvironment;
	if (inString == "timeOfDay") return jsspell::eTimeOfDay;
	if (inString == "castingAnim") return jsspell::eCastingAnim;
	if (inString == "cancelOnSit") return jsspell::eCancelOnSit;
	if (inString == "name") return jsspell::eName;
	if (inString == "target") return jsspell::eTarget;
	if (inString == "noRemove") return jsspell::eNoRemove;
	if (inString == "description") return jsspell::eDescription;

}


void GetSpellAttribute(v8::Local<v8::Name> name,
	const v8::PropertyCallbackInfo<v8::Value>& info) {

	v8::Local<v8::External> ext = info.Data().As<v8::External>();
	PSPELL spell_info = static_cast<PSPELL>(ext->Value());
	auto isolate = info.GetIsolate();
	v8::Local<v8::Value> val = cast_to_js<const char*>(info, "Undefined");
	v8::String::Utf8Value accessor(info.GetIsolate(), name);

	switch (hashit(std::string(*accessor))) {
	case jsspell::eRange:
		val = cast_to_js(info, spell_info->Range);
		break;
	case jsspell::eAERange:
		val = cast_to_js(info, spell_info->AERange);
		break;
	case jsspell::ePushBack:
		val = cast_to_js(info, spell_info->PushBack);
		break;
	case jsspell::ePushUp:
		val = cast_to_js(info, spell_info->PushUp);
		break;
	case jsspell::eCastTime:
		val = cast_to_js(info, spell_info->CastTime);
		break;
	case jsspell::eRecoveryTime:
		val = cast_to_js(info, spell_info->RecoveryTime);
		break;
	case jsspell::eRecastTime:
		val = cast_to_js(info, spell_info->RecastTime);
		break;
	case jsspell::eDurationType:
		val = cast_to_js(info, spell_info->DurationType);
		break;
	case jsspell::eDurationCap:
		val = cast_to_js(info, spell_info->DurationCap);
		break;
	case jsspell::eManaCost:
		val = cast_to_js(info, spell_info->ManaCost);
		break;
	case jsspell::eReagentID:
		val = cast_to_js(info, spell_info->ReagentID);
		break;
	case jsspell::eReagentCount:
		val = cast_to_js(info, spell_info->ReagentCount);
		break;
	case jsspell::eNoExpendReagent:
		val = cast_to_js(info, spell_info->NoExpendReagent);
		break;
	case jsspell::eCalcIndex:
		val = cast_to_js(info, spell_info->CalcIndex);
		break;
	case jsspell::eNumEffects:
		val = cast_to_js(info, spell_info->NumEffects);
		break;
	case jsspell::eBookIcon:
		val = cast_to_js(info, spell_info->BookIcon);
		break;
	case jsspell::eGemIcon:
		val = cast_to_js(info, spell_info->GemIcon);
		break;
	case jsspell::eDescriptionIndex:
		val = cast_to_js(info, spell_info->DescriptionIndex);
		break;
	case jsspell::eResistAdj:
		val = cast_to_js(info, spell_info->ResistAdj);
		break;
	case jsspell::eDeity:
		val = cast_to_js(info, spell_info->Diety);
		break;
	case jsspell::eSpellAnim:
		val = cast_to_js(info, spell_info->SpellAnim);
		break;
	case jsspell::eSpellIcon:
		val = cast_to_js(info, spell_info->SpellIcon);
		break;
	case jsspell::eDurationParticleEffect:
		val = cast_to_js(info, spell_info->DurationParticleEffect);
		break;
	case jsspell::eId:
		val = cast_to_js(info, spell_info->ID);
		break;
	case jsspell::eHateMod:
		val = cast_to_js(info, spell_info->HateMod);
		break;
	case jsspell::eResistPerLevel:
		val = cast_to_js(info, spell_info->ResistPerLevel);
		break;
	case jsspell::eResistCap:
		val = cast_to_js(info, spell_info->ResistCap);
		break;
	case jsspell::eEnduranceCost:
		val = cast_to_js(info, spell_info->EnduranceCost);
		break;
	case jsspell::eEnduranceValue:
		val = cast_to_js(info, spell_info->EnduranceValue);
		break;
	case jsspell::eHateGenerated:
		val = cast_to_js(info, spell_info->HateGenerated);
		break;
	case jsspell::eHitCountType:
		val = cast_to_js(info, spell_info->HitCountType);
		break;
	case jsspell::eHitCount:
		val = cast_to_js(info, spell_info->HitCount);
		break;
	case jsspell::eConeStartAngle:
		val = cast_to_js(info, spell_info->ConeStartAngle);
		break;
	case jsspell::eConeEndAngle:
		val = cast_to_js(info, spell_info->ConeEndAngle);
		break;
	case jsspell::ePvpResistBase:
		val = cast_to_js(info, spell_info->PvPResistBase);
		break;
	case jsspell::ePvpCalc:
		val = cast_to_js(info, spell_info->PvPCalc);
		break;
	case jsspell::ePvpResistCap:
		val = cast_to_js(info, spell_info->PvPResistCap);
		break;
	case jsspell::ePvpDuration:
		val = cast_to_js(info, spell_info->PvPDuration);
		break;
	case jsspell::eSpellGroup:
		val = cast_to_js(info, spell_info->SpellGroup);
		break;
	case jsspell::eSpellSubGroup:
		val = cast_to_js(info, spell_info->SpellSubGroup);
		break;
	case jsspell::eSpellRank:
		val = cast_to_js(info, spell_info->SpellRank);
		break;
	case jsspell::eMaxResist:
		val = cast_to_js(info, spell_info->MaxResist);
		break;
	case jsspell::eMinResist:
		val = cast_to_js(info, spell_info->MinResist);
		break;
	case jsspell::eSpreadRadius:
		val = cast_to_js(info, spell_info->SpreadRadius);
		break;
	case jsspell::eCritChanceOverride:
		val = cast_to_js(info, spell_info->CritChanceOverride);
		break;
	case jsspell::eMaxTargets:
		val = cast_to_js(info, spell_info->MaxTargets);
		break;
	case jsspell::eMinRange:
		val = cast_to_js(info, spell_info->MinRange);
		break;
	case jsspell::eUninterruptable:
		val = cast_to_js(info, spell_info->Uninterruptable);
		break;
	case jsspell::eNotStackableDot:
		val = cast_to_js(info, spell_info->NotStackableDot);
		break;
	case jsspell::eDeletable:
		val = cast_to_js(info, spell_info->Deletable);
		break;
	case jsspell::eBypassRegenCheck:
		val = cast_to_js(info, spell_info->BypassRegenCheck);
		break;
	case jsspell::eCanCastInCombat:
		val = cast_to_js(info, spell_info->CanCastInCombat);
		break;
	case jsspell::eCanCastOutOfCombat:
		val = cast_to_js(info, spell_info->CanCastOutOfCombat);
		break;
	case jsspell::eCastNotStanding:
		val = cast_to_js(info, spell_info->CastNotStanding);
		break;
	case jsspell::eCanMgb:
		val = cast_to_js(info, spell_info->CanMGB);
		break;
	case jsspell::eNoDispell:
		val = cast_to_js(info, spell_info->NoDisspell);
		break;
	case jsspell::eIsSkill:
		val = cast_to_js(info, spell_info->IsSkill);
		break;
	case jsspell::eStacksWithDiscs:
		val = cast_to_js(info, spell_info->bStacksWithDiscs);
		break;
	case jsspell::eSpellType:
		val = cast_to_js(info, spell_info->SpellType);
		break;
	case jsspell::eResist:
		val = cast_to_js(info, spell_info->Resist);
		break;
	case jsspell::eTargetType:
		val = cast_to_js(info, spell_info->TargetType);
		break;
	case jsspell::eCastDifficulty:
		val = cast_to_js(info, spell_info->CastDifficulty);
		break;
	case jsspell::eSkill:
		val = cast_to_js(info, spell_info->Skill);
		break;
	case jsspell::eZoneType:
		val = cast_to_js(info, spell_info->ZoneType);
		break;
	case jsspell::eEnvironment:
		val = cast_to_js(info, spell_info->Environment);
		break;
	case jsspell::eTimeOfDay:
		val = cast_to_js(info, spell_info->TimeOfDay);
		break;
	case jsspell::eCastingAnim:
		val = cast_to_js(info, spell_info->CastingAnim);
		break;
	case jsspell::eCancelOnSit:
		val = cast_to_js(info, spell_info->CancelOnSit);
		break;
	case jsspell::eName:
		val = cast_to_js(info, spell_info->Name);
		break;
	case jsspell::eTarget:
		val = cast_to_js(info, spell_info->Target);
		break;
	case jsspell::eNoRemove:
		val = cast_to_js(info, spell_info->NoRemove);
		break;
	case jsspell::eDescription:
		val = cast_to_js(info, pCDBStr->GetString(spell_info->DescriptionIndex, 6, NULL));
		break;

	}

	info.GetReturnValue().Set(val);

}

v8::Local<v8::Value> GetSpell(PSPELL spell, const v8::PropertyCallbackInfo<Value>& info) {
	auto isolate = info.GetIsolate();
	auto context = isolate->GetCurrentContext();
	auto obj = v8::Object::New(isolate);
	auto ext = v8::External::New(isolate, spell);

	if (spell == nullptr) {
		return v8::Null(isolate);

	}
	std::function<void(const char*)> set_accessor =
		[context, obj, isolate, ext](
			const char* accessor) -> void {
		obj->SetAccessor(context,
			GetLocalName(isolate, accessor),
			GetSpellAttribute, nullptr, ext);
	};

	std::vector<std::string> attribute_list{ 
		"range","aERange","pushBack","pushUp","castTime","recoveryTime","recastTime","durationType","durationCap","manaCost","reagentID","reagentCount","noExpendReagent","calcIndex","numEffects","bookIcon","gemIcon","descriptionIndex","resistAdj","deity","spellAnim","spellIcon","durationParticleEffect","id","hateMod","resistPerLevel","resistCap","enduranceCost","enduranceValue","hateGenerated","hitCountType","hitCount","coneStartAngle","coneEndAngle","pvpResistBase","pvpCalc","pvpResistCap","pvpDuration","spellGroup","spellSubGroup","spellRank","maxResist","minResist","spreadRadius","critChanceOverride","maxTargets","minRange","uninterruptable","notStackableDot","deletable","bypassRegenCheck","canCastInCombat","canCastOutOfCombat","castNotStanding","canMgb","noDispell","isSkill","stacksWithDiscs","spellType","resist","targetType","castDifficulty","skill","zoneType","environment","timeOfDay","castingAnim","cancelOnSit","name","target","noRemove","description"
	};
	for (std::string attribute : attribute_list) {
		set_accessor(attribute.c_str());
	}
	return obj;
}
