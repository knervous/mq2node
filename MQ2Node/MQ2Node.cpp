// MQ2Node.cpp : Defines the entry point for the DLL application.
//

// PLUGIN_API is only to be used for callbacks.  All existing callbacks at this time
// are shown below. Remove the ones your plugin does not use.  Always use Initialize
// and Shutdown for setup and cleanup, do NOT do it in DllMain.

#include "NodeRuntime.h"
#include "MQ2Plugin.h"
#include <thread>
#include <chrono>

std::unique_ptr<NodeRuntime> node_runtime;
std::thread runtime_thread;


// DLL Properties Linker General Output: ..\..\$(Configuration)\$(ProjectName)$(TargetExt)
// EXE $(ProjectDir)..\nodejs\out\Release\MQ2Node$(TargetExt)
//int main(int argc, char** argv) {
//	// Testing code for node runtime
//	return 0;
//}


PreSetup("MQ2Node");
char NodeSection[MAX_STRING] = "MQ2NODE";

void StopAndJoin() {
	if (node_runtime->GetIsRunning()) {
		node_runtime->StopExecution();
		runtime_thread.join();
	}
}


VOID NodeEval(PSPAWNINFO pChar, PCHAR szLine)
{
	node_runtime->SetRunningScript(std::string(szLine));
	
	StopAndJoin();
	runtime_thread = std::thread([&]() {
		node_runtime->Eval();
	});
}

VOID NodeStop(PSPAWNINFO pChar, PCHAR szLine)
{
	StopAndJoin();
}

VOID NodeLoad(PSPAWNINFO pCHar, PCHAR szLine) {
	CHAR file_name[MAX_STRING] = { 0 };

	sprintf(file_name, "%s\\%s", gszMacroPath, szLine);
	if (szLine[0] == 0) {
		SyntaxError("Usage: /node <filename *.js>");
		std::string str("Tried this path: ");
		str += file_name;
		SyntaxError((PCHAR)str.c_str());
		return;
	}

	StopAndJoin();
	
	runtime_thread = std::thread([&]() {
		node_runtime->ModuleLoad(file_name);
	});

}

// Called once, when the plugin is to initialize
PLUGIN_API VOID InitializePlugin(VOID)
{
	DebugSpewAlways("Initializing MQ2Node");
	if (node_runtime == nullptr) {
		node_runtime = std::make_unique<NodeRuntime>();
		node_runtime->Init();
	}

	//Add commands, MQ2Data items, hooks, etc.
	AddCommand("/noderun", NodeEval, 0, 0);
	AddCommand("/node", NodeLoad);
	AddCommand("/nodestop", NodeStop);
	//AddXMLFile("MQUI_MyXMLFile.xml");
	//bmMyBenchmark=AddMQ2Benchmark("My Benchmark Name");
}

// Called once, when the plugin is to shutdown
PLUGIN_API VOID ShutdownPlugin(VOID)
{
	if (node_runtime != nullptr) {
		StopAndJoin();
		node_runtime->Dispose();
		node_runtime.release();
	}

	DebugSpewAlways("Shutting down MQ2Node");
	RemoveCommand("/noderun");
	RemoveCommand("/node");
	RemoveCommand("/nodestop");
	//Remove commands, MQ2Data items, hooks, etc.
	//RemoveMQ2Benchmark(bmMyBenchmark);
	//RemoveCommand("/mycommand");
	//RemoveXMLFile("MQUI_MyXMLFile.xml");
}

// Called after entering a new zone
PLUGIN_API VOID OnZoned(VOID)
{
	DebugSpewAlways("MQ2Node::OnZoned()");
}

// Called once directly before shutdown of the new ui system, and also
// every time the game calls CDisplay::CleanGameUI()
PLUGIN_API VOID OnCleanUI(VOID)
{
	DebugSpewAlways("MQ2Node::OnCleanUI()");
	// destroy custom windows, etc
}

// Called once directly after the game ui is reloaded, after issuing /loadskin
PLUGIN_API VOID OnReloadUI(VOID)
{
	DebugSpewAlways("MQ2Node::OnReloadUI()");
	// recreate custom windows, etc
}

// Called every frame that the "HUD" is drawn -- e.g. net status / packet loss bar
PLUGIN_API VOID OnDrawHUD(VOID)
{
	// DONT leave in this debugspew, even if you leave in all the others
	//DebugSpewAlways("MQ2Node::OnDrawHUD()");
}

// Called once directly after initialization, and then every time the gamestate changes
PLUGIN_API VOID SetGameState(DWORD GameState)
{
	DebugSpewAlways("MQ2Node::SetGameState()");
	//if (GameState==GAMESTATE_INGAME)
	// create custom windows if theyre not set up, etc
}


// This is called every time MQ pulses
PLUGIN_API VOID OnPulse(VOID)
{
	// DONT leave in this debugspew, even if you leave in all the others
	//DebugSpewAlways("MQ2Node::OnPulse()");
}

// This is called every time WriteChatColor is called by MQ2Main or any plugin,
// IGNORING FILTERS, IF YOU NEED THEM MAKE SURE TO IMPLEMENT THEM. IF YOU DONT
// CALL CEverQuest::dsp_chat MAKE SURE TO IMPLEMENT EVENTS HERE (for chat plugins)
PLUGIN_API DWORD OnWriteChatColor(PCHAR Line, DWORD Color, DWORD Filter)
{
	DebugSpewAlways("MQ2Node::OnWriteChatColor(%s)", Line);
	return 0;
}

// This is called every time EQ shows a line of chat with CEverQuest::dsp_chat,
// but after MQ filters and chat events are taken care of.
PLUGIN_API DWORD OnIncomingChat(PCHAR Line, DWORD Color)
{
	DebugSpewAlways("MQ2Node::OnIncomingChat(%s)", Line);
	if (node_runtime != nullptr) {
		node_runtime->OnChatEvent(Line);
	}
	return 0;
}

// This is called each time a spawn is added to a zone (inserted into EQ's list of spawns),
// or for each existing spawn when a plugin first initializes
// NOTE: When you zone, these will come BEFORE OnZoned
PLUGIN_API VOID OnAddSpawn(PSPAWNINFO pNewSpawn)
{
	if (node_runtime != nullptr) {
		node_runtime->OnSpawnEvent(pNewSpawn, "spawn");
	}
	DebugSpewAlways("MQ2Node::OnAddSpawn(%s)", pNewSpawn->Name);
}

// This is called each time a spawn is removed from a zone (removed from EQ's list of spawns).
// It is NOT called for each existing spawn when a plugin shuts down.
PLUGIN_API VOID OnRemoveSpawn(PSPAWNINFO pSpawn)
{
	if (node_runtime != nullptr) {
		node_runtime->OnSpawnEvent(pSpawn, "despawn");
	}
	DebugSpewAlways("MQ2Node::OnRemoveSpawn(%s)", pSpawn->Name);
}

// This is called each time a ground item is added to a zone
// or for each existing ground item when a plugin first initializes
// NOTE: When you zone, these will come BEFORE OnZoned
PLUGIN_API VOID OnAddGroundItem(PGROUNDITEM pNewGroundItem)
{
	DebugSpewAlways("MQ2Node::OnAddGroundItem(%d)", pNewGroundItem->DropID);
}

// This is called each time a ground item is removed from a zone
// It is NOT called for each existing ground item when a plugin shuts down.
PLUGIN_API VOID OnRemoveGroundItem(PGROUNDITEM pGroundItem)
{
	DebugSpewAlways("MQ2Node::OnRemoveGroundItem(%d)", pGroundItem->DropID);
}

// This is called when we receive the EQ_BEGIN_ZONE packet is received
PLUGIN_API VOID OnBeginZone(VOID)
{
	if (node_runtime != nullptr) {
		node_runtime->OnZoneEvent("begin");
	}
	DebugSpewAlways("MQ2Node::OnBeginZone");
}

// This is called when we receive the EQ_END_ZONE packet is received
PLUGIN_API VOID OnEndZone(VOID)
{
	if (node_runtime != nullptr) {
		node_runtime->OnZoneEvent("end");
	}
	DebugSpewAlways("MQ2Node::OnEndZone");
}
// This is called when pChar!=pCharOld && We are NOT zoning
// honestly I have no idea if its better to use this one or EndZone (above)
PLUGIN_API VOID Zoned(VOID)
{
	DebugSpewAlways("MQ2Node::Zoned");
}
