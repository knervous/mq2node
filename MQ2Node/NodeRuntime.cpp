#include "NodeRuntime.h"
#include <thread>
#include "Global.h"
#include "node_process.h"


v8::Persistent<Context> context_;
v8::Isolate* isolate_;
uv_async_t async_;
uv_loop_t loop_;


void NodeRuntime::StopExecution() {
	if (script_running_) {
		WriteChatColor("Stopping execution");
	}
	allow_execution_ = false;
	struct payload* p = new payload();
	p->type = new std::string("stop");
	async_.data = (void*)p;
	uv_async_send(&async_);
}

void NodeRuntime::Init() {
	std::vector<std::string> args{ "None" };
	std::vector<std::string> exec_args{};
	std::vector<std::string> errors;
	int exit_code = node::InitializeNodeWithArgs(&args, &exec_args, &errors);
	for (const std::string& error : errors)
		fprintf(stderr, "%s: %s\n", args[0].c_str(), error.c_str());
	if (exit_code != 0) {
		return;
	}

	platform_ = MultiIsolatePlatform::Create(4);
	V8::InitializePlatform(platform_.get());
	V8::Initialize();
}

void NodeRuntime::Dispose() {
	try {
		{
			V8::Dispose();
			V8::ShutdownPlatform();

			platform_->~MultiIsolatePlatform();
			allocator_.reset();
			isolate_ = nullptr;
			halt_exec_ = nullptr;

			WriteChatColor("Shut down V8 engine.");
		}

	}
	catch (...) {
		WriteChatColor("Error calling dispose");
	}
}

void NodeRuntime::ModuleLoad(const char* module_path) {
	return Eval(true, module_path);
}

void NodeRuntime::SetRunningScript(std::string eval) {
	running_script_ = eval;
}

void NodeRuntime::OnZoneEvent(const char* type) {
	if (!script_running_) {
		return;
	}
	struct payload* p = new payload();
	p->data = new std::string(type);
	p->type = new std::string("zone");
	async_.data = (void*)p;
	uv_async_send(&async_);
}

void NodeRuntime::OnChatEvent(const char* line) {
	if (!script_running_) {
		return;
	}
	struct payload* p = new payload();
	p->data = new std::string(line);
	p->type = new std::string("chat");
	async_.data = (void*)p;
	uv_async_send(&async_);
}



void NodeRuntime::OnSpawnEvent(PSPAWNINFO spawn, const char* type) {
	if (!script_running_) {
		return;
	}
	struct payload* p = new payload();
	p->data = new PSPAWNINFO(spawn);
	p->type = new std::string(type);
	async_.data = (void*)p;
	uv_async_send(&async_);
}

void DoCallback(uv_async_t* handle) {
	{
		v8::Unlocker unlock(isolate_);
		{
			Locker lock(isolate_);
			Isolate::Scope isolate_scope(isolate_);
			v8::HandleScope scope(isolate_);
			auto context = isolate_->GetCurrentContext();
			auto global = context->Global();

			struct payload* p = (struct payload*)handle->data;
			auto type = (std::string) * p->type;
			try {
				if (type == "stop") {
					uv_close((uv_handle_t*)&async_, NULL);
					uv_stop(&loop_);
				}
				else if (type == "spawn" || type == "despawn") {
					auto spawn = (PSPAWNINFO*)p->data;
					Local<Value> args[] = { GetSpawn(*spawn, isolate_) };
					v8::Local<v8::Value> spawn_listener;
					if (!global->Get(
						context, GetLocalName(isolate_, type == "spawn" ? "spawnListener" : "despawnListener")).ToLocal(&spawn_listener)) {
						return;
					}
					spawn_listener.As<v8::Function>()->Call(context, context->Global(), 1, args);
				}
				else if (type == "zone") {
					auto zone_event = (std::string*)p->data;
					Local<Value> args[] = { GetLocalName(isolate_, zone_event->c_str()) };
					v8::Local<v8::Value> zone_listener;
					if (!global->Get(
						context, GetLocalName(isolate_, "zoneListener")).ToLocal(&zone_listener)) {
						return;
					}
					zone_listener.As<v8::Function>()->Call(context, context->Global(), 1, args);
				}
				else if (type == "chat") {
					auto chat_text = (std::string*)p->data;
					Local<Value> args[] = { GetLocalName(isolate_, chat_text->c_str()) };
					v8::Local<v8::Value> chat_listener;
					if (!global->Get(
						context, GetLocalName(isolate_, "chatListener")).ToLocal(&chat_listener)) {
						return;
					}
					chat_listener.As<v8::Function>()->Call(context, context->Global(), 1, args);
				}
			}
			catch (...) {}

			delete p->data;
			delete p->type;
			delete p;
		}
	}


};


void NodeRuntime::Eval(bool module_load, const char* module_path) {
	module_path_ = std::string(module_path == nullptr ? "" : module_path);
	module_load_ = module_load;
	allow_execution_ = true;
	{
		int ret = uv_loop_init(&loop_);

		allocator_ = ArrayBufferAllocator::Create();
		isolate_ = NewIsolate(allocator_, &loop_, platform_.get());

		isolate_->SetFatalErrorHandler([](const char* location, const char* message) -> void {
			WriteChatColor("Error occured in script");
			WriteChatColor((PCHAR)location);
			WriteChatColor((PCHAR)message);
			std::printf(message);
		});
		{
			Locker lock(isolate_);
			Isolate::Scope isolate_scope(isolate_);

			std::unique_ptr<IsolateData, decltype(&node::FreeIsolateData)>
				isolate_data(node::CreateIsolateData(
					isolate_, &loop_, platform_.get(), allocator_.get()),
					node::FreeIsolateData);
			HandleScope handle_scope(isolate_);

			Local<Context> context =
				node::NewContext(isolate_, GetObjectTemplate(isolate_, this));



			const std::vector<std::string>& args{ "", "--no-hard-abort" };
			const std::vector<std::string>& exec_args{};

			const std::vector<std::string>& module_args{
				"",
				module_path_,
				"--no-hard-abort"
			};

			//node::PatchProcessObject()
			Context::Scope context_scope(context);

			std::unique_ptr<Environment, decltype(&node::FreeEnvironment)> env(
				node::CreateEnvironment(isolate_data.get(),
					context,
					module_load_ ? module_args : args,
					exec_args),
				node::FreeEnvironment);

			TryCatchScope try_catch(env.get());


			// Prepend local require
			std::string inject = "";
			std::string global_require(gszMacroPath);
			std::replace(global_require.begin(), global_require.end(), '\\', '/');
			global_require += "/";
			inject =
				inject + "const publicRequire = require('module').createRequire(`" +
				global_require.c_str() + "`); globalThis.require = publicRequire;";

			inject += running_script_.c_str();

			MaybeLocal<Value> loadenv_ret =
				module_load_
				? node::LoadEnvironment(env.get(), node::StartExecutionCallback{})
				: node::LoadEnvironment(env.get(), inject.c_str());

			if (try_catch.HasCaught()) {
				WriteChatColor("Caught exception");
				String::Utf8Value exception(
					isolate_,
					try_catch.Exception()->ToString(context).ToLocalChecked());
				String::Utf8Value stack_trace(
					isolate_, try_catch.StackTrace(context).ToLocalChecked());
				WriteChatColor(*exception);
				WriteChatColor(*stack_trace);
				script_running_ = false;
				return;
			}

			script_running_ = true;

			isolate_->AddMessageListener([](Local<v8::Message> message, Local<Value> data) -> void {
				String::Utf8Value utf8_message(v8::Isolate::GetCurrent(), message->Get());
				std::string line = "Error occurred on line ";
				line += std::to_string(message->GetLineNumber(v8::Isolate::GetCurrent()->GetCurrentContext()).ToChecked());
				WriteChatColor((PCHAR)*utf8_message);
				WriteChatColor((PCHAR)line.c_str());
			});
			String::Utf8Value utf8(isolate_, loadenv_ret.ToLocalChecked());

			WriteChatColor(*utf8);
			std::printf(*utf8);
			{
				SealHandleScope seal(isolate_);
				bool more = false;
				uv_async_init(&loop_, &async_, DoCallback);
				do {
					uv_run(&loop_, UV_RUN_DEFAULT);
					this->platform_->DrainTasks(isolate_);
					more = uv_loop_alive(&loop_);
					if (more) continue;
					node::EmitBeforeExit(env.get());
					more = uv_loop_alive(&loop_);
				} while (allow_execution_ && more == true);
				int exit_code = node::EmitExit(env.get());

				node::Stop(env.get());
			}
		}

		bool platform_finished = false;
		platform_->AddIsolateFinishedCallback(
			isolate_,
			[](void* data) { *static_cast<bool*>(data) = true; },
			&platform_finished);
		platform_->UnregisterIsolate(isolate_);
		isolate_->Dispose();

		// Wait until the platform has cleaned up all relevant resources.
		while (!platform_finished) uv_run(&loop_, UV_RUN_ONCE);
		uv_loop_close(&loop_);

		// Clear and cleanup
		script_running_ = false;
		WriteChatColor("Finished");
		halt_exec_ = nullptr;
	}
}
