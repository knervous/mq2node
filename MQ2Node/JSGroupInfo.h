#pragma once
#include "NodeRuntime.h"

v8::Local<v8::Object> GetGroupInfo(GROUPINFO* group_info, const v8::PropertyCallbackInfo<Value>& info);

v8::Local<v8::Value> GetGroupMember(GROUPMEMBER* group_info, const v8::PropertyCallbackInfo<Value>& info);

