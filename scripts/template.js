const {
	Conditions,
	ConditionResult,
	getConditionResult,
	waitForAny,
} = require("./js/conditions");

const {
	mq,
	stop,
	main,
	sleep,
	getClosestOfSpawnNames,
	useDebugger,
} = require("./js/mq");

const { addSpawnListener } = require("./js/events");
const { doIHaveBuff } = require("./js/util");

// Main loop
main(async () => {
	// All my logic goes here
	// This will be repeated forever until `stop()` is called or /nodestop from the client

});

// Uncomment if you want to debug
// useDebugger();
