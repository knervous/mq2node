import { Spawn } from "./mq";

/**
 * @summary Types of conditions to wait for
 */
declare enum Conditions {
	TargetDied = targetDied,
	IDied = iDied,
	IZoned = iZoned,
	WithinTargetRange = withinTargetRange,
	TimeElapsed = timeElapsed,
}

/**
 * @summary Description of what happened for a certain event
 */
declare enum ConditionResult {
	HadNoTarget = 0,
	TargetDied = 1,
	IDied = 2,
	IZoned = 3,
	ReachedTarget = 4,
	TargetDespawned = 5,
	SwitchedTargets = 6,
	TimedOut = 7,
}

declare function withinTargetRange(distance: number): Promise<ConditionResult>;
declare function timeElapsed(seconds: number): Promise<ConditionResult>;
declare function targetDied(spawn: Spawn): Promise<ConditionResult>;
declare function iDied(): Promise<ConditionResult>;
declare function iZoned(): Promise<ConditionResult>;

/**
 * @param  {...Condition} conditions
 * @summary Wait for any of the parameters to finish no matter the order
 */
export function waitForAny(...conditions: Array<Conditions>): ConditionResult;

/**
 * @param  {...Condition} conditions
 * @summary Wait for all of the parameters to finish
 */
export function waitForAll(...conditions: Array<Conditions>): ConditionResult;

/**
 * @param {ConditionResult} result
 * Description of the condition result
 */
export function getConditionResult(result): string;
