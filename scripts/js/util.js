const {mq} = require('./mq');

function doIHaveBuff(partialBuffName) {
    return mq.me.buffs.some((buff) => buff.spell.name.includes(partialBuffName))
}

module.exports = {
    doIHaveBuff
}