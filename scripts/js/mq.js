const mq = global.mq;
const stop = () => mq.commands.run("nodestop");

// private
function distance(pt1, pt2) {
	var diffX = pt1.x - pt2.x;
	var diffY = pt1.y - pt2.y;
	return Math.sqrt(diffX * diffX + diffY * diffY);
}

function makePoint(spawn) {
	return {
		x: spawn.x,
		y: spawn.y,
	};
}

// exported
function sleep(ms = 1000) {
	return new Promise((res) => setTimeout(res, ms));
}

function distanceBetweenSpawns(spawn1, spawn2) {
	if (!spawn1 || !spawn2) {
		return Infinity;
	}
	return distance(makePoint(spawn1), makePoint(spawn2));
}

async function main(fn, loopSpeed = 50) {
	while (true) {
		await fn();
		await sleep(loopSpeed);
	}
}

function getClosestOfSpawnNames(names) {
	if (!Array.isArray(names)) {
		mq.warn("Bad params, arg 1 should be an array of strings");
    }
    const myLoc = makePoint(mq.me.spawn);
	return names
		.map(name => mq.getNthNearestSpawn(name, 1))
		.filter(Boolean)
		.reduce((next, current) => {
			current.distanceFromMe = distance(myLoc, makePoint(current));
			if (next === null) {
				return current;
			}
			next.distanceFromMe = distance(myLoc, makePoint(next));
			return current.distanceFromMe < next.distanceFromMe ? current : next;
		}, null);
}

function useDebugger(port = 9229){
    const inspector = require("inspector");
    inspector.open(port, "127.0.0.1");
}

module.exports = {
	mq,
    stop,
    useDebugger,
	main,
	sleep,
	getClosestOfSpawnNames,
	distanceBetweenSpawns,
};
