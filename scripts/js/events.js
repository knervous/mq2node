require('./mq')

let spawnListeners = [],
	despawnListeners = [],
	zoneListeners = [],
	chatListeners = [];

// Spawn
 function addSpawnListener(listener) {
	spawnListeners.push(listener);
}
 function removeSpawnListener(listener) {
	spawnListeners = spawnListeners.filter((l) => l !== listener);
}

// Despawn
 function addDespawnListener(listener) {
	despawnListeners.push(listener);
}
 function removeDespawnListener(listener) {
	despawnListeners = despawnListeners.filter((l) => l !== listener);
}

// Zone
 function addZoneListener(listener) {
	zoneListeners.push(listener);
}
 function removeZoneListener(listener) {
	zoneListeners = despawnzoneListenersListeners.filter((l) => l !== listener);
}

// Chat
 function addChatListener(listener) {
	chatListeners.push(listener);
}
 function removeChatListener(listener) {
	chatListeners = chatListeners.filter((l) => l !== listener);
}

global.spawnListener = function (spawn) {
	spawnListeners.forEach((fn) => fn(spawn));
};

global.despawnListener = function (spawn) {
	despawnListeners.forEach((fn) => fn(spawn));
};

global.zoneListener = function (type) {
	zoneListeners.forEach((fn) => fn(type));
};

global.chatListener = function (line) {
	chatListeners.forEach((fn) => fn(line));
};

module.exports = {
    addSpawnListener, removeSpawnListener,
    addChatListener, removeChatListener,
    addDespawnListener, removeDespawnListener,
    addZoneListener, removeZoneListener
}