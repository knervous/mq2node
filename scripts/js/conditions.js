const { mq, distanceBetweenSpawns, sleep } = require("./mq");
const {
	addDespawnListener,
	removeDespawnListener,
	addZoneListener,
	removeZoneListener,
} = require("./events");
const { SpawnTypes } = require("./constants");

const Conditions = {
	TargetDied: targetDied,
	IDied: iDied,
	IZoned: iZoned,
	WithinTargetRange: withinTargetRange,
	TimeElapsed: timeElapsed,
};

const ConditionResult = {
	HadNoTarget: 0,
	TargetDied: 1,
	IDied: 2,
	IZoned: 3,
	ReachedTarget: 4,
	TargetDespawned: 5,
	SwitchedTargets: 6,
	TimedOut: 7,
};

function promiseAny(promises) {
	return Promise.all(
		promises.map((promise) =>
			promise.then(
				(val) => {
					throw val;
				},
				(reason) => reason
			)
		)
	).then(
		(reasons) => {
			throw reasons;
		},
		(firstResolved) => firstResolved
	);
}

function withinTargetRange(spawn = {}, distance = 200) {
	return new Promise(async (res) => {
		if (!spawn) {
			// May lag behind for targetting if we just targeted something
			await sleep(150);
		}

		const targetId = spawn?.spawnId ?? mq.me?.target?.spawnId;
		if (!targetId) {
			res(ConditionResult.HadNoTarget);
		}
		const interval = setInterval(() => {
			const spawn = mq.getSpawnById(targetId);
			if (!spawn) {
				res(ConditionResult.TargetDespawned);
				clearInterval(interval);
			}
			if (spawn?.spawnId !== mq.me.target?.spawnId) {
				res(ConditionResult.SwitchedTargets);
				clearInterval(interval);
			}
			if (distanceBetweenSpawns(mq.me.spawn, spawn) <= distance) {
				res(ConditionResult.ReachedTarget);
				clearInterval(interval);
			}
		}, 100);
	});
}

function timeElapsed(seconds) {
	return new Promise((res) => {
		let cancelled = true;
		setTimeout(() => {
			res(ConditionResult.TimedOut);
		}, seconds * 1000);
	});
}

function targetDied(spawn) {
	const spawnId = spawn?.spawnId ?? mq?.me?.target?.spawnId;
	if (mq.getSpawnById(spawnId)?.type === SpawnTypes.CORPSE) {
		return Conditions.TargetDied;
	}
	if (!spawnId) {
		return Conditions.HadNoTarget;
	}

	return new Promise((res, _rej) => {
		const despawnListener = (spawn) => {
			if (spawn?.spawnId === spawnId) {
				res(Conditions.TargetDied);
				removeDespawnListener(despawnListener);
			}
		};
		addDespawnListener(despawnListener);

		const interval = setInterval(() => {
			if (mq.getSpawnById(spawnId)?.type === SpawnTypes.CORPSE) {
				res(ConditionResult.TargetDied);
				removeDespawnListener(despawnListener);
				clearInterval(interval);
			}
		}, 100);
	});
}

function iDied() {
	return iZoned();
}

function iZoned() {
	return new Promise((res, _rej) => {
		const zoneListener = (type) => {
			if (type === "begin") {
				res(ConditionResult.IZoned);
				removeZoneListener(zoneListener);
			}
		};
		addZoneListener(zoneListener);
	});
}

// Reuse any and all, also allow to pass in invoked func with params
// for promise or just the function itself to be invoked and return a promise
async function _waitFor(type, ...conditions) {
	const filteredConditions = conditions
		.map((c) => (typeof c?.then === "function" ? c : conditions[c]))
		.filter(Boolean);
	const ret = await (type === "any"
		? promiseAny(filteredConditions)
		: Promise.all(filteredConditions));
	return ret;
}

function waitForAny(...conditions) {
	return _waitFor("any", ...conditions);
}

function waitForAll(...conditions) {
	return _waitFor("all", ...conditions);
}

function getConditionResult(result) {
	return (
		Object.entries(ConditionResult).find(
			([_key, val]) => val === result
		)?.[0] ?? "Unknown"
	);
}

module.exports = {
	Conditions,
	ConditionResult,
	getConditionResult,
	waitForAny,
	waitForAll,
};
