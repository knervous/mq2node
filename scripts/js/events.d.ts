import { Spawn } from "./mq";

type SpawnListener = (spawn: Spawn) => void;
type ChatListener = (line: string) => void;
/**
 * @param type = 'begin' | 'end'
 */
type ZoneListener = (type: string) => void;

// Spawn
export function addSpawnListener(listener : SpawnListener) : void;

export function removeSpawnListener(listener : SpawnListener) : void;

// Despawn
export function addDespawnListener(listener : SpawnListener) : void;
export function removeDespawnListener(listener : SpawnListener) : void;

// Zone
export function addZoneListener(listener : ZoneListener) : void;
export function removeZoneListener(listener : ZoneListener) : void;
// Chat
export function addChatListener(listener: ChatListener) : void;
export function removeChatListener(listener: ChatListener) : void;