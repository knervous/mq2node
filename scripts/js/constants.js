const SpawnTypes = {
	PC: 0,
	NPC: 1,
	CORPSE: 2,
};

module.exports = {
	SpawnTypes,
};
