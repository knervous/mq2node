// To load this in the client, make sure the plugin is loaded with /plugin mq2node
// And then /node example-qeynos
// These are our utility functions - you can create your own and import them in any other file.
const {
	Conditions,
	ConditionResult,
	getConditionResult,
	waitForAny,
} = require("./js/conditions");

const {
	mq,
	stop,
	main,
	sleep,
	getClosestOfSpawnNames,
	useDebugger,
} = require("./js/mq");

const { addSpawnListener, addZoneListener } = require("./js/events");
const { doIHaveBuff } = require("./js/util");

const killTracker = {};

// Add a spawn listener to echo to chat when something pops
addSpawnListener(spawn => {
	mq.log(`We've got a spawn: ${spawn.displayedName}`)
});

// Main loop
main(async () => {
	// Tell everyone in BC to follow me
	mq.run(`bca //target ${mq.me.name}`);
	mq.run(`bca //stick 5`);

	// Stop sticking if I had a target
	mq.run("stick off");

	// Check buffs - this example is from a bard
	if (!doIHaveBuff("Selo")) {
		mq.run("melody 1 2 3");
	}
	// List of NPCs I want to kill
	await findAndKillNpcs(["a decaying skeleton", "a large rat"]);

	// Sleep for a bit
	await sleep(1500);
});

// Call this to allow the file to be debugged while it's running
useDebugger();

async function findAndKillNpcs(npcs) {
	// Find the closest NPC out of the names I passed in from above
	const closestNpc = getClosestOfSpawnNames(npcs);
	if (closestNpc) {
		// spawn.displayedName will be Priest of Discord and is better for viewing
		const name = closestNpc.displayedName;
		mq.log(`Time to die, ${name}`);

		// spawn.name will be priest_of_discord00 and is better for targeting
		mq.run(`target ${closestNpc.name}`);
		mq.run(`stick 10`);

		// Wait for any of the below conditions to finish
		// Either we get within target range of 25 or 10 seconds pass
		// Then we get the result of whatever happened
		const stickResult = await waitForAny(
			Conditions.WithinTargetRange(closestNpc, 25),
			Conditions.TimeElapsed(10)
		);

		if (stickResult !== ConditionResult.ReachedTarget) {
			mq.log(`Something else happened: ${getConditionResult(stickResult)}`);
			return stickResult;
		}

		// Tell our dudes to attack what we're attacking
		mq.run(`bca //assist ${mq.me.name}`);
		mq.run(`bca //stick 5`);

		// Attack ourselves
		mq.run(`attack`);

		// Wait until something happens during the fight
		// Either the target died, 30 seconds pass, or I died
		const fightResult = await waitForAny(
			Conditions.TargetDied(closestNpc),
			Conditions.TimeElapsed(30),
			Conditions.IDied,
			Conditions.IZoned
		);

		// Now we evaluate what happened
		switch (fightResult) {
			case ConditionResult.TargetDied:
				killTracker[name] = (killTracker[name] || 0) + 1;
				mq.log(`Another one bites the dust! Here are our kill stats:`);
				mq.log(JSON.stringify(killTracker, null, 4));
				break;
			case ConditionResult.TimedOut:
				mq.log("This is taking awhile!");
				break;
			case ConditionResult.IZoned:
			case ConditionResult.IDied:
				mq.warn(`Better luck next time!`);
				stop();
				break;
		}

		// Rinse and repeat - this will automatically return and go back to the main loop
	} else {
		mq.log(`Rats, we're all out of spawns!`);
	}
}
