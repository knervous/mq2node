<!--lint disable no-literal-urls-->
## MQ2Node


### This project includes a fork of the NodeJS repository.

## Overview

This plugin is intended to expose MQ2's core data / methods / events in the NodeJS runtime. It can be used to evaluate single lines or load modules that may depend on any number of other modules. There can only be one running instance at any given time to avoid conflicts.

## Building
- Build the solution in the nodejs directory node.sln in release mode via Visual Studio or by running the `nodebuild.bat` and then manually building the project MQ2Node
- Make sure the MQ2Node project is linked in the VanillaDIY.sln
- You may have to rename the output/$(Configuration) folder from the node.sln build from Release to Debug due to a VS bug with linked projects
- Build the MQ2Node project in VanillaDIY.sln
- MQ2Node.dll should be generated in $(SolutionDir)/Release
- Scripts should be copied in ~/Release/Macros

## Commands

- /node \<path\>
    - Load a javascript file and run as a macro.
    - Example: `/node my-macro.js` OR just `/node my-macro`
- /noderun \<evaluated script>
    - Evaluates Javascript and outputs the value
    - Example `/noderun return mq.me.spawn.displayedName`
        - Will print "CharacterName" in the MQ2ChatWnd
- /nodestop
    - Stops running Node macro
    - Useful if the JS module has logic to continue work in the event loop i.e. setInterval
    - Runs before any new execution automatically


## Data Structures
- In progress and will be included in a typescript definition for easy creation of macros with intellisense

## Example macros
- Included in the scripts folder

## Contributing / testing
- If you are interesting in this project for developing or testing feel free to reach out at pcj174@gmail.com



